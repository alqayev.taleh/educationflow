/**
 * 
 */
package wot.flow.edu.gov.az.persistence.service;

import java.util.List;

import wot.flow.edu.gov.az.bean.aas.registerOrUpdatePerson.RegisterOrUpdatePersonReqBean;
import wot.flow.edu.gov.az.persistence.entities.aas.PersonEnt;

/**
 * @author Taleh-PC
 * 2017 Nov 4, 2017 2:17:29 PM
 */
public interface PersonService {
	public PersonEnt findByOidAndState (final Long oid , final Integer state) ;

	/**
	 * @param person
	 * @return
	 */
	public PersonEnt registerNewPersonAccount(RegisterOrUpdatePersonReqBean person);
	
	public List<PersonEnt> getPersonsBySate(Integer state,Integer page,Integer size);

	/**
	 * @param fin
	 * @return
	 */
	public PersonEnt findPersonByFinOrIdCardsNo(String fin,String idCardsNo);
	
	
}
