/**
 * 
 */
package wot.flow.edu.gov.az.bean.aas.addParent;

import java.util.Set;

import wot.flow.edu.gov.az.bean.aas.NotificationDto;

/**
 * @author Alqayev Taleh
 *
 * 2017 Dec 9, 2017 11:46:33 PM
 */
public class ParentBean {
private Long id;
	
	private Long parentId;
	
	private Long childId;
	
	private String email;
	
	private String mobile;
	
	private String home;
	
	private Long parentType;
	
	private Set<NotificationDto> notifications;
	
	public Set<NotificationDto> getNotifications() {
		return notifications;
	}

	public void setNotifications(Set<NotificationDto> notifications) {
		this.notifications = notifications;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getParentId() {
		return parentId;
	}

	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}

	public Long getChildId() {
		return childId;
	}

	public void setChildId(Long childId) {
		this.childId = childId;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getHome() {
		return home;
	}

	public void setHome(String home) {
		this.home = home;
	}

	public Long getParentType() {
		return parentType;
	}

	public void setParentType(Long parentType) {
		this.parentType = parentType;
	}
}
