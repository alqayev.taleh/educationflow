package wot.flow.edu.gov.az.config.security.filter;

import java.io.IOException;
import java.util.Objects;
import java.util.Optional;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.web.filter.OncePerRequestFilter;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.MalformedJwtException;
import wot.flow.edu.gov.az.config.security.jwt.JwtTokenUtil;
import wot.flow.edu.gov.az.config.security.jwt.JwtUser;
import wot.flow.edu.gov.az.persistence.service.CustomUserDetailsService;
import wot.flow.edu.gov.az.utilities.bean.Response;
import wot.flow.edu.gov.az.utilities.enums.ErrorHandl;
import wot.flow.edu.gov.az.web.exception.IllegalArgumentInJwtException;
import wot.flow.edu.gov.az.web.exception.InvalidHeaderException;




/**
 * @author Alqayev Taleh
 *
 * 2017 Nov 9, 2017 5:44:32 PM
 */
public class AuthenticationTokenFilter extends OncePerRequestFilter {

	private final Log logger = LogFactory.getLog(this.getClass());

	@Autowired
	private CustomUserDetailsService userDetailsService;

	@Autowired
	private JwtTokenUtil jwtTokenUtil;

	@Autowired
	@Qualifier("responseBean")
	private Response response;

	@Autowired
	private ObjectMapper mapper;

	@Value("${jwt.header}")
	private String tokenHeader;

	private final String authHeader = "authentication";
	
	private final String authHeaderValue = "authentication";

	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws ServletException, IOException {

		final String requestTokenHeader = request.getHeader(this.tokenHeader);
		final String requestAuthHeader = request.getHeader(this.authHeader);

		String username = null;
		String token = null;

		boolean isAuth = Objects.nonNull(requestAuthHeader) && !requestAuthHeader.isEmpty() && requestAuthHeader.equals(authHeaderValue);
		boolean isAuthorization = Objects.nonNull(requestTokenHeader) && !requestTokenHeader.isEmpty();
		try {
//			if ( Objects.nonNull(requestAuthHeader) && !requestAuthHeader.isEmpty() && requestAuthHeader.equals(authHeaderValue)){
//				username = requestAuthHeader;
//				JwtUser userDetails = userDetailsService.loadUserByUsername(username);
//				UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities());
//				authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
//				SecurityContextHolder.getContext().setAuthentication(authentication);
//			}
			if (isAuth || isAuthorization) {
				if (isAuthorization){
					token = requestTokenHeader;
					try {
						username = jwtTokenUtil.getUsernameFromToken(token);
					} catch (MalformedJwtException e) {
						logger.error("an error occured during getting username from token", e);
						throw new IllegalArgumentInJwtException();
					} catch (ExpiredJwtException e) {
						logger.warn("***** the token is expired and not valid anymore ******", e);
						throw new wot.flow.edu.gov.az.web.exception.ExpiredJwtException();
					}
				}
			} 
			//ignore token when login , replace Bearer 'token' -> auth 'username'
			
			else {
				throw new InvalidHeaderException();
			}

			if (username != null && SecurityContextHolder.getContext().getAuthentication() == null) {

				JwtUser userDetails = this.userDetailsService.loadUserByUsername(username);

				// For simple validation it is completely sufficient to just check the token integrity. You don't have to call
				// the database compellingly. 
				if (jwtTokenUtil.validateToken(token, userDetails)) {
					UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities());
					authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
					SecurityContextHolder.getContext().setAuthentication(authentication);
				}
			}
			chain.doFilter(request, response);
		} catch (RuntimeException e) {
			/* HANDLE ALL EXCEPTION IN FILTER */
			if (e instanceof wot.flow.edu.gov.az.web.exception.UserNotFoundException) {
				this.response.setCode(ErrorHandl.USER_NOT_FOUND.getErrorCode());
				this.response.setMessage(ErrorHandl.USER_NOT_FOUND.toString());
			} else if (e instanceof wot.flow.edu.gov.az.web.exception.InvalidHeaderException) {
				this.response.setCode(ErrorHandl.INVALID_HEADER.getErrorCode());
				this.response.setMessage(ErrorHandl.INVALID_HEADER.toString());
			} else if (e instanceof  wot.flow.edu.gov.az.web.exception.IllegalArgumentInJwtException) {
				this.response.setCode(ErrorHandl.ILLEGAL_ARGUMENT_IN_JWT.getErrorCode());
				this.response.setMessage(ErrorHandl.ILLEGAL_ARGUMENT_IN_JWT.toString());
			} else if (e instanceof wot.flow.edu.gov.az.web.exception.ExpiredJwtException) {
				this.response.setCode(ErrorHandl.EXPIRED_JWT.getErrorCode());
				this.response.setMessage(ErrorHandl.EXPIRED_JWT.toString());
			} else if (e instanceof wot.flow.edu.gov.az.web.exception.UserLockedException) {
				this.response.setCode(ErrorHandl.ACCOUNT_LOCKED.getErrorCode());
				this.response.setMessage(ErrorHandl.ACCOUNT_LOCKED.toString());
			} else if (e instanceof wot.flow.edu.gov.az.web.exception.UserEnabledException) {
				this.response.setCode(ErrorHandl.ACCOUNT_ENABLED.getErrorCode());
				this.response.setMessage(ErrorHandl.ACCOUNT_ENABLED.toString());
			}  else {
				this.response.setCode(666);
				this.response.setMessage("internal server error");
			}
			response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
			response.getWriter().write(convertObjectToJson(this.response));
		}
	}

	/*create response json*/
	public String convertObjectToJson(Object object) throws JsonProcessingException {
		Optional<Object> opt = Optional.ofNullable(object);
		if (!opt.isPresent()) return null;
		return mapper.writeValueAsString(object);
	}
}