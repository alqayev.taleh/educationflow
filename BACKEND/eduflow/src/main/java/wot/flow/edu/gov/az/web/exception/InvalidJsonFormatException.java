/**
 * 
 */
package wot.flow.edu.gov.az.web.exception;

/**
 * @author Alqayev Taleh
 *
 * 2017 Nov 10, 2017 10:57:15 PM
 */
public class InvalidJsonFormatException extends RuntimeException {

	private static final long serialVersionUID = 1363312971559580524L;

	public InvalidJsonFormatException() {
        super();
    }

    public InvalidJsonFormatException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public InvalidJsonFormatException(final String message) {
        super(message);
    }

    public InvalidJsonFormatException(final Throwable cause) {
        super(cause);
    }	
	
	
}
