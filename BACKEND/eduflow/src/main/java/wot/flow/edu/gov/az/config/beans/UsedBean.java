/**
 * 
 */
package wot.flow.edu.gov.az.config.beans;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import com.fasterxml.jackson.databind.ObjectMapper;

import wot.flow.edu.gov.az.ExampleClass;
import wot.flow.edu.gov.az.utilities.bean.Response;

/**
 * @author Taleh-PC
 * 2017 Nov 9, 2017 2:47:49 PM
 */
@Configuration
public class UsedBean {

	@Bean(name = "responseBean")
	public Response responseBean (){
		return new Response();
	}
	
	@Bean
	public ObjectMapper mapper () {
		return new ObjectMapper();
	}
	
	@Bean
	@Profile("DEV")
	public ExampleClass appDev(){
		System.out.println("***************Appppppppppppp  DEV**************");
		return new ExampleClass("DEV");
		
	}
	
	@Bean
	@Profile("ALFA")
	public ExampleClass appAlfa(){
		System.out.println("***************Appppppppppppp  ALFAAAAAA**************");
		return new ExampleClass("ALFA");
		
	}
}
