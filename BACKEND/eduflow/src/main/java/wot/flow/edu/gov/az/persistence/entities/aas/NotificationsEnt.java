package wot.flow.edu.gov.az.persistence.entities.aas;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import wot.flow.edu.gov.az.persistence.entities.BaseEnt;

@Entity
@Table(schema = "aas" , name = "aas_notifications")
public class NotificationsEnt extends BaseEnt{
	
	@Column(name = "n_name")
	private String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "Notifications [name=" + name + "]";
	}
}
