/**
 * 
 */
package wot.flow.edu.gov.az.web.exception;

/**
 * @author Alqayev Taleh
 *
 * 2017 Nov 18, 2017 10:23:07 PM
 */
public class DictionaryDetailNotFoundException extends RuntimeException {

	private static final long serialVersionUID = 1363312971559580524L;
	
	public DictionaryDetailNotFoundException() {
        super();
    }

    public DictionaryDetailNotFoundException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public DictionaryDetailNotFoundException(final String message) {
        super(message);
    }

    public DictionaryDetailNotFoundException(final Throwable cause) {
        super(cause);
    }	
}
