/**
 * 
 */
package wot.flow.edu.gov.az.config.security.jwt;

/**
 * @author Taleh-PC
 * 2017 Nov 5, 2017 7:05:18 PM
 */
public class JwtPerson {
	private final Long pOid;
	private final String pName;
	private final String pSurname;
	private final String pMiddleName;
	private final String pCitizenship;
	private final String pFin;
	private final String pIdCardsNo;
	private final String pGender;
	private final String pBirthday;
	public JwtPerson(Long pOid, String pName, String pSurname, String pMiddleName, String pCitizenship, String pFin,
			String pIdCardsNo, String pGender, String pBirthday) {
		super();
		this.pOid = pOid;
		this.pName = pName;
		this.pSurname = pSurname;
		this.pMiddleName = pMiddleName;
		this.pCitizenship = pCitizenship;
		this.pFin = pFin;
		this.pIdCardsNo = pIdCardsNo;
		this.pGender = pGender;
		this.pBirthday = pBirthday;
	}
	public Long getpOid() {
		return pOid;
	}
	public String getpName() {
		return pName;
	}
	public String getpSurname() {
		return pSurname;
	}
	public String getpMiddleName() {
		return pMiddleName;
	}
	public String getpCitizenship() {
		return pCitizenship;
	}
	public String getpFin() {
		return pFin;
	}
	public String getpIdCardsNo() {
		return pIdCardsNo;
	}
	public String getpGender() {
		return pGender;
	}
	public String getpBirthday() {
		return pBirthday;
	}
	
	
}
