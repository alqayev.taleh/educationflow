/**
 * 
 */
package wot.flow.edu.gov.az.config.beans;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

import wot.flow.edu.gov.az.persistence.entities.aas.PersonEnt;
import wot.flow.edu.gov.az.persistence.entities.aas.UserEnt;

/**
 * @author Alqayev Taleh
 *
 * 2017 Nov 10, 2017 11:10:52 PM
 */
@Configuration
//@Scope("prototype")
public class EntBeans {

	@Bean(name = "person")
	@Scope("prototype")
	public PersonEnt personEnt () {
		return new PersonEnt();
	}
	
	@Bean(name = "person")
	@Scope("prototype")
	public UserEnt userEnt () {
		return new UserEnt();
	}
}
