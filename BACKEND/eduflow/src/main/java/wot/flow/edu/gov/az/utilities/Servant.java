/**
 * 
 */
package wot.flow.edu.gov.az.utilities;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import wot.flow.edu.gov.az.web.exception.WrongDateException;

/**
 * @author Alqayev Taleh
 *
 * 2017 Nov 10, 2017 11:36:40 PM
 */
public class Servant {
	public static String format (String date , String input , String output ) {
		try {
			return LocalDate.parse(date, DateTimeFormatter.ofPattern(input)).format(DateTimeFormatter.ofPattern(output));
		} catch (Exception e) {
			throw new WrongDateException();
		}
	}
	public static String createdDate (){
		return LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyyMMddHHmmssSSS"));
	}
}
