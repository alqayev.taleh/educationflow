/**
 * 
 */
package wot.flow.edu.gov.az.bean.aas;

/**
 * @author Taleh-PC
 * 2017 Nov 5, 2017 7:13:18 PM
 */
public class NotificationDto {
	private Long oid;
	private String name;
	
	
	
	public NotificationDto() {
	}
	public NotificationDto(Long oid, String name) {
		super();
		this.oid = oid;
		this.name = name;
	}
	public Long getOid() {
		return oid;
	}
	public void setOid(Long oid) {
		this.oid = oid;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	@Override
	public String toString() {
		return "NotificationsDto [oid=" + oid + ", name=" + name + "]";
	}


}
