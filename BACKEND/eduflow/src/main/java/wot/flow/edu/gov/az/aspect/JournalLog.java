/**
 * 
 */
package wot.flow.edu.gov.az.aspect;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

/**
 * @author Alqayev Taleh
 *
 * 2017 Dec 11, 2017 12:43:28 AM
 */
@Aspect
@Component
public class JournalLog {
	
	long time=0;

	private final Log logger = LogFactory.getLog(this.getClass());

	@Before("@annotation(wot.flow.edu.gov.az.utilities.Timed) && execution(public * *(..))")
	public void beforeLogAuthenticationTokenFilter() throws Throwable {
		HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder
				.currentRequestAttributes())
				.getRequest();
		long startTime = System.currentTimeMillis();
		String serviceName =  request.getRequestURI();
		logger.info("\n***** START TIME : "+startTime+" -> "+serviceName+" <- ****** ");
		time = startTime;
                System.out.println("test jenkins");
	}

	@After("@annotation(wot.flow.edu.gov.az.utilities.Timed) && execution(public * *(..))")
	public void afterLogAuthenticationTokenFilter() throws Throwable {
		HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder
				.currentRequestAttributes())
				.getRequest();
		long endTime = System.currentTimeMillis();
		String serviceName =  request.getRequestURI();
		logger.info("\n***** End TIME : "+endTime+"-> "+serviceName+" <- ****** SUM : "+(endTime-time));
	}
}
