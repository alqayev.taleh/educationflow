/**
 * 
 */
package wot.flow.edu.gov.az.web.exception;

/**
 * @author Alqayev Taleh
 *
 * 2017 Nov 11, 2017 2:39:19 PM
 */
public class FillIdCardsNoOrFinException extends RuntimeException{

	private static final long serialVersionUID = 1363312971559580524L;
	
	public FillIdCardsNoOrFinException() {
        super();
    }

    public FillIdCardsNoOrFinException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public FillIdCardsNoOrFinException(final String message) {
        super(message);
    }

    public FillIdCardsNoOrFinException(final Throwable cause) {
        super(cause);
    }	
}