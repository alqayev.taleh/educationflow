/**
 * 
 */
package wot.flow.edu.gov.az.persistence.service.impl;

import java.util.HashSet;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import wot.flow.edu.gov.az.bean.aas.NotificationDto;
import wot.flow.edu.gov.az.bean.aas.addParent.AddParentReqBean;
import wot.flow.edu.gov.az.persistence.dao.DictionaryDetailRepository;
import wot.flow.edu.gov.az.persistence.dao.ParentRepository;
import wot.flow.edu.gov.az.persistence.dao.PersonRepository;
import wot.flow.edu.gov.az.persistence.entities.aas.ParentEnt;
import wot.flow.edu.gov.az.persistence.entities.aas.UserEnt;
import wot.flow.edu.gov.az.persistence.entities.admin.DictionaryDetailEnt;
import wot.flow.edu.gov.az.persistence.service.ParentService;
import wot.flow.edu.gov.az.utilities.Servant;
import wot.flow.edu.gov.az.utilities.enums.UTILENUMS;
import wot.flow.edu.gov.az.web.exception.DictionaryDetailNotFoundException;
import wot.flow.edu.gov.az.web.exception.PersonNotFoundException;

/**
 * @author Alqayev Taleh
 *
 * 2017 Nov 18, 2017 10:07:14 PM
 */
@Service(value = "parentService")
@Transactional
public class ParentServiceImpl implements ParentService {

	@Autowired
	private ParentRepository parentRepository;
	
	@Autowired 
	private PersonRepository personRepository;
	
	@Autowired 
	private DictionaryDetailRepository dictionaryDetailRepository;

	/* (non-Javadoc)
	 * @see wot.flow.edu.gov.az.persistence.service.ParentService#addOrUpdateParent(wot.flow.edu.gov.az.bean.aas.addParent.AddParentReqBean)
	 */
	@Override
	public void addOrUpdateParent(AddParentReqBean requestBean) {
		
		ParentEnt parent = null;
		
		if (Objects.isNull(requestBean.getParent().getId())) {
			parent = new ParentEnt();
			parent.setAuthId(requestBean.getAuthId());
			parent.setChild(Optional.ofNullable(personRepository.findOne(requestBean.getParent().getChildId())).orElseThrow(PersonNotFoundException::new));
			parent.setCreateDate(Servant.createdDate());
			parent.setEmail(requestBean.getParent().getEmail());
			parent.setHome(requestBean.getParent().getHome());
			parent.setMobile(requestBean.getParent().getMobile());
			parent.setParent(Optional.ofNullable(personRepository.findOne(requestBean.getParent().getParentId())).orElseThrow(PersonNotFoundException::new));
			parent.setParentType(Optional.ofNullable(dictionaryDetailRepository.findOne(requestBean.getParent().getParentType())).orElseThrow(DictionaryDetailNotFoundException::new));
			parent.setState(UTILENUMS.ACTIVE.getValue());
			if(Objects.nonNull(requestBean.getParent().getNotifications()) && !requestBean.getParent().getNotifications().isEmpty())
			parent.setParentToNotifications(Optional.ofNullable(dictionaryDetailRepository.findByIdIn(requestBean.getParent().getNotifications().stream().map(notf -> notf.getOid()).collect(Collectors.toSet()))).orElseThrow(DictionaryDetailNotFoundException::new));
			parentRepository.save(parent);
		} else {
			parent = parentRepository.findOne(requestBean.getParent().getId());
			parent.setAuthId(requestBean.getAuthId());
			parent.setChild(Optional.ofNullable(personRepository.findOne(requestBean.getParent().getChildId())).orElseThrow(PersonNotFoundException::new));
			parent.setCreateDate(Servant.createdDate());
			parent.setEmail(requestBean.getParent().getEmail());
			parent.setHome(requestBean.getParent().getHome());
			parent.setMobile(requestBean.getParent().getMobile());
			parent.setParent(Optional.ofNullable(personRepository.findOne(requestBean.getParent().getParentId())).orElseThrow(PersonNotFoundException::new));
			parent.setParentType(Optional.ofNullable(dictionaryDetailRepository.findOne(requestBean.getParent().getParentType())).orElseThrow(DictionaryDetailNotFoundException::new));
			parent.setState(UTILENUMS.ACTIVE.getValue());
			parentRepository.save(parent);
		}
	}

	/* (non-Javadoc)
	 * @see wot.flow.edu.gov.az.persistence.service.ParentService#changeNotification(java.lang.Long, java.util.Set)
	 */
	@Override
	public void changeNotification(Long id, Set<NotificationDto> newNotifications) {
		
		ParentEnt user =  parentRepository.findOne(id);

		Set<DictionaryDetailEnt> userNotifications = user.getParentToNotifications();

		Set<DictionaryDetailEnt> addNotifications = dictionaryDetailRepository.findByIdIn(
				newNotifications.stream()
				.map(NotificationDto::getOid)
				.collect(Collectors.toSet()));

		Set<DictionaryDetailEnt> removeNotifications = null;
		if (Optional.ofNullable(userNotifications).isPresent());
		removeNotifications = userNotifications.stream()
				.filter(notification -> addNotifications.add(notification))
				.collect(Collectors.toSet());

		Set<DictionaryDetailEnt> allNotifications = new HashSet<>(userNotifications);
		allNotifications.addAll(addNotifications);

		user.setParentToNotifications(allNotifications);
		removeNotifications.forEach(u ->{
			user.getParentToNotifications().remove(u);
		});
		parentRepository.save(user);
	}
}
