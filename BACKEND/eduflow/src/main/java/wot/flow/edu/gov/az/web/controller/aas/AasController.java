package wot.flow.edu.gov.az.web.controller.aas;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import wot.flow.edu.gov.az.bean.aas.PersonDto;
import wot.flow.edu.gov.az.bean.aas.UserDto;
import wot.flow.edu.gov.az.bean.aas.addParent.AddParentReqBean;
import wot.flow.edu.gov.az.bean.aas.addParent.AddParentRespBean;
import wot.flow.edu.gov.az.bean.aas.auth.AuthReqBean;
import wot.flow.edu.gov.az.bean.aas.auth.AuthRespBean;
import wot.flow.edu.gov.az.bean.aas.changeProfilePhoto.ChangeProfilePhotoReqBean;
import wot.flow.edu.gov.az.bean.aas.changeProfilePhoto.ChangeProfilePhotoRespBean;
import wot.flow.edu.gov.az.bean.aas.changeUserNotification.ChangeUserNotificationReqBean;
import wot.flow.edu.gov.az.bean.aas.changeUserNotification.ChangeUserNotificationRespBean;
import wot.flow.edu.gov.az.bean.aas.changeUserRole.ChangeUserRoleReqBean;
import wot.flow.edu.gov.az.bean.aas.changeUserRole.ChangeUserRoleRespBean;
import wot.flow.edu.gov.az.bean.aas.getInfoAfterLogin.AfterLoginBean;
import wot.flow.edu.gov.az.bean.aas.getInfoAfterLogin.GetInfoAfterLoginReqBean;
import wot.flow.edu.gov.az.bean.aas.getInfoAfterLogin.GetInfoAfterLoginRespBean;
import wot.flow.edu.gov.az.bean.aas.getPersonByFin.GetPersonByFinReqBean;
import wot.flow.edu.gov.az.bean.aas.getPersonByFin.GetPersonByFinRespBean;
import wot.flow.edu.gov.az.bean.aas.getPersons.GetPersonsReqBean;
import wot.flow.edu.gov.az.bean.aas.getPersons.GetPersonsRespBean;
import wot.flow.edu.gov.az.bean.aas.getUserByUserId.GetUserByUserIdReqBean;
import wot.flow.edu.gov.az.bean.aas.getUserByUserId.GetUserByUserIdRespBean;
import wot.flow.edu.gov.az.bean.aas.getUsersByFinOrIdCardsNo.GetUsersByFinOrIdCardsNoReqBean;
import wot.flow.edu.gov.az.bean.aas.getUsersByFinOrIdCardsNo.GetUsersByFinOrIdCardsNoRespBean;
import wot.flow.edu.gov.az.bean.aas.registerOrUpdatePerson.RegisterOrUpdatePersonReqBean;
import wot.flow.edu.gov.az.bean.aas.registerOrUpdatePerson.RegisterOrUpdatePersonRespBean;
import wot.flow.edu.gov.az.bean.aas.registerOrUpdateUser.RegisterOrUpdateUserReqBean;
import wot.flow.edu.gov.az.bean.aas.registerOrUpdateUser.RegisterOrUpdateUserRespBean;
import wot.flow.edu.gov.az.bean.aas.updatePassword.UpdatePasswordReqBean;
import wot.flow.edu.gov.az.bean.aas.updatePassword.UpdatePasswordRespBean;
import wot.flow.edu.gov.az.config.mailListener.CustomApplicationEvent;
import wot.flow.edu.gov.az.config.security.jwt.JwtTokenUtil;
import wot.flow.edu.gov.az.config.security.jwt.JwtUser;
import wot.flow.edu.gov.az.config.security.override.CustomAuthenticationProvider;
import wot.flow.edu.gov.az.persistence.dao.PersonRepository;
import wot.flow.edu.gov.az.persistence.entities.aas.UserEnt;
import wot.flow.edu.gov.az.persistence.service.ParentService;
import wot.flow.edu.gov.az.persistence.service.PersonService;
import wot.flow.edu.gov.az.persistence.service.UserService;
import wot.flow.edu.gov.az.utilities.Servant;
import wot.flow.edu.gov.az.utilities.Timed;
import wot.flow.edu.gov.az.utilities.enums.DATEFORMAT;
import wot.flow.edu.gov.az.utilities.enums.UTILENUMS;
import wot.flow.edu.gov.az.web.exception.BadCredentialsException;
import wot.flow.edu.gov.az.web.exception.InvalidJsonFormatException;
import wot.flow.edu.gov.az.web.exception.UserNotFoundException;


@RestController()
@RequestMapping(path = "/aas")
public class AasController {
	private final Log logger = LogFactory.getLog(this.getClass());
	//ResponseBeans
	@Autowired
	private RegisterOrUpdatePersonRespBean successRegisterOrUpdatePersonRespBean;
	
	@Autowired
	private RegisterOrUpdateUserRespBean successRegisterOrUpdateUserRespBean;
	
	@Autowired
	private UpdatePasswordRespBean updatePasswordRespBean;

	@Autowired
	private GetPersonsRespBean getPersonsRespBean;
	
	@Autowired
	private GetPersonByFinRespBean getPersonByFinRespBean;
	
	@Autowired
	GetUserByUserIdRespBean getUserByUserIdRespBean;
	
	@Autowired
	ChangeUserRoleRespBean changeUserRole;
	
	@Autowired
	private ChangeUserNotificationRespBean changeUserNotificationRespBean;
	
	@Autowired
	private GetUsersByFinOrIdCardsNoRespBean getUsersByFinOrIdCardsNoRespBean;
	
	@Autowired
	private AddParentRespBean addParentRespBean;
	
	@Autowired 
	private ChangeProfilePhotoRespBean changeProfilePhoto;

	@Autowired
	GetInfoAfterLoginRespBean getInfoAfterLogin;
//	private final Logger log = LoggerFactory.getLogger(getClass());

	@Autowired
	private UserDetailsService userDetailsService;
	
	@Autowired
	private PersonService personService;
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private ParentService parentService;
	
	@Autowired
	private PersonRepository personRepository;
	
	@Autowired
    ApplicationEventPublisher eventPublisher;

	@Autowired
	private CustomAuthenticationProvider authManager;

	@Autowired
	private JwtTokenUtil jwtTokenUtil;

	@Value("${jwt.header}")
	private String tokenHeader;
	
	@PostMapping(value = "/auth")
	@Timed
//	@PreAuthorize("hasRole('ADMIN')")
	public ResponseEntity<AuthRespBean> createAuthenticationToken(@RequestBody AuthReqBean requestBean,
			HttpServletRequest request, HttpServletResponse response) throws AuthenticationException, IOException {

		UsernamePasswordAuthenticationToken tokenAuth = new UsernamePasswordAuthenticationToken(
				requestBean.getUsername(), requestBean.getPassword());

		final Authentication authentication = authManager.authenticate(tokenAuth);
		
		if (!authentication.isAuthenticated()) {
			throw new BadCredentialsException();
		}
//		SecurityContextHolder.getContext().setAuthentication(authentication);

		final JwtUser userDetails = (JwtUser) authentication.getPrincipal();
		final String token = jwtTokenUtil.generateToken(userDetails);
		
		return ResponseEntity.ok(new AuthRespBean(token));
	}
	
	@GetMapping("/logout")
	public ResponseEntity<?> logout(HttpServletRequest request) {
		
		String token = request.getHeader(tokenHeader).substring(7);
		
		String disabledToken = jwtTokenUtil.disabledToken(token);
		
		return ResponseEntity.ok(new AuthRespBean(disabledToken));
	}


	@PutMapping("/refreshToken")
	public ResponseEntity<?> refreshToken(HttpServletRequest request) {
		
		String token = request.getHeader(tokenHeader);
		
		String refreshedToken = jwtTokenUtil.refreshToken(token);
		
		return ResponseEntity.ok(new AuthRespBean(refreshedToken));
	}

	@PostMapping(value = "/registerOrUpdatePerson")
	public ResponseEntity<RegisterOrUpdatePersonRespBean> registerOrUpdatePerson(@RequestBody RegisterOrUpdatePersonReqBean requestBean) throws AuthenticationException, IOException {
		
		Optional.ofNullable(requestBean).orElseThrow(InvalidJsonFormatException::new);
		
		personService.registerNewPersonAccount(requestBean);
		
		return ResponseEntity.ok(successRegisterOrUpdatePersonRespBean);
	}
	
	@PostMapping(value = "/registerOrUpdateUser")
	public ResponseEntity<RegisterOrUpdateUserRespBean> registerOrUpdateUser(@RequestBody RegisterOrUpdateUserReqBean requestBean,HttpServletRequest request) throws AuthenticationException, IOException {
		
		Optional.ofNullable(requestBean).orElseThrow(InvalidJsonFormatException::new);
		
		UserEnt userEnt = userService.registerNewUserAccount(requestBean);
		
		eventPublisher.publishEvent(new CustomApplicationEvent(userEnt, request.getLocale()));
		
		return ResponseEntity.ok(successRegisterOrUpdateUserRespBean);
	}

	
	
	@GetMapping(value = "/getPersons/{page}/{size}")
	public ResponseEntity<GetPersonsRespBean> getPersons(@PathVariable("page") Integer page,@PathVariable("size") Integer size)  {
		
		List<PersonDto> persons = personService.getPersonsBySate(UTILENUMS.ACTIVE.getValue(),page, size).stream()
				.map(personEnt -> new PersonDto(personEnt.getId()
						, personEnt.getBirthday()
						, personEnt.getCitizenship()
						, personEnt.getFin()
						, personEnt.getGender()
						, personEnt.getIdCardsNo()
						, personEnt.getMiddlename()
						, personEnt.getName()
						, personEnt.getSurname()))
				.collect(Collectors.toList());
		getPersonsRespBean.setPersons(persons);
		return ResponseEntity.ok(getPersonsRespBean);
	}
	
	@PostMapping(value = "/getPersonByFin")
	public ResponseEntity<GetPersonByFinRespBean> getPersonByFin(@RequestBody GetPersonByFinReqBean requestBean)  {
		
		PersonDto person = Optional.of(personService.findPersonByFinOrIdCardsNo(requestBean.getFin(), requestBean.getIdCardsNo()))
				.map(personEnt -> new PersonDto(
						personEnt.getId()
						, Servant.format(personEnt.getBirthday(),DATEFORMAT.DATABASE_DATE.getDate(),DATEFORMAT.FRONT_DATE.getDate())
						, personEnt.getCitizenship()
						, personEnt.getFin()
						, personEnt.getGender()
						, personEnt.getIdCardsNo()
						, personEnt.getMiddlename()
						, personEnt.getName()
						, personEnt.getSurname())).get();
		getPersonByFinRespBean.setPerson(person);
		return ResponseEntity.ok(getPersonByFinRespBean);
	}
	
	@PostMapping(value = "/getUserByUserId")
	public ResponseEntity<GetUserByUserIdRespBean> getUserById(@RequestBody GetUserByUserIdReqBean requestBean)  {
		;
		UserDto user = Optional.ofNullable(userService.findUserByUserId(requestBean.getId()))
				.map(userEnt -> new UserDto(
						userEnt.getId() 
						, userEnt.getEmail()
						, userEnt.getUsername()
						, Objects.isNull(userEnt.getPersonType())?0:userEnt.getPersonType().getId()
						, userEnt.isAccountNonLocked()
						, userEnt.isEnabled()
						, Optional.ofNullable(userEnt.getRoles()).orElse(new HashSet<>()).stream()
											.map(roleEnt -> roleEnt.getId())
											.collect(Collectors.toSet())
						, Optional.ofNullable(userEnt.getUserNotifications()).orElse(new HashSet<>()).stream()
											.map(notificationEnt -> notificationEnt.getId())
											.collect(Collectors.toSet())))
				        .get();
		
		getUserByUserIdRespBean.setUser(user);
		return ResponseEntity.ok(getUserByUserIdRespBean);
	}
	
	@PostMapping(value = "/getUsersByFinOrIdCardsNo")
	public ResponseEntity<GetUsersByFinOrIdCardsNoRespBean> getUsersByFinOrIdCardsNo(@RequestBody GetUsersByFinOrIdCardsNoReqBean requestBean)  {
		
		Set<UserDto> users = userService.findUsersByFinOrIdCardsNo(requestBean.getFinOrIdCardsNo()).stream()
				.map(user -> new UserDto(
						user.getId()
						, user.getEmail()
						, user.getEmail()
						, Objects.isNull(user.getPersonType())?0:user.getPersonType().getId()
						, user.isAccountNonLocked()
						, user.isEnabled()
						, user.getRoles().stream()
						                .map(role -> role.getId())
						                .collect(Collectors.toSet())
						, user.getUserNotifications().stream()
						                            .map(notification -> notification.getId())
						                            .collect(Collectors.toSet()))).collect(Collectors.toSet());
		
		
		getUsersByFinOrIdCardsNoRespBean.setUsers(users);
		return ResponseEntity.ok(getUsersByFinOrIdCardsNoRespBean);
	}
	
	@PostMapping(value = "/changeUserRole")
	public ResponseEntity<ChangeUserRoleRespBean> changeUserRole(@RequestBody ChangeUserRoleReqBean requestBean)  {
		
		userService.changeRole(requestBean.getId(),Optional.ofNullable(requestBean.getRoles()).orElseThrow(InvalidJsonFormatException::new));
		
		return ResponseEntity.ok(changeUserRole);
	}
	
	@PostMapping(value = "/changeUserNotification")
	public ResponseEntity<ChangeUserNotificationRespBean> changeUserNotification(@RequestBody ChangeUserNotificationReqBean requestBean)  {
		
		userService.changeNotification(requestBean.getId(),Optional.ofNullable(requestBean.getNotifications()).orElseThrow(InvalidJsonFormatException::new));
		
		return ResponseEntity.ok(changeUserNotificationRespBean);
	}
	
	@PostMapping(value = "/changeParentNotification")
	public ResponseEntity<ChangeUserNotificationRespBean> changeParentNotification(@RequestBody ChangeUserNotificationReqBean requestBean)  {
		
		parentService.changeNotification(requestBean.getId(),Optional.ofNullable(requestBean.getNotifications()).orElseThrow(InvalidJsonFormatException::new));
		
		return ResponseEntity.ok(changeUserNotificationRespBean);
	}
	
	@PutMapping(value = "/updatePassword")
	public ResponseEntity<UpdatePasswordRespBean> updatePassword(HttpServletRequest request,@RequestBody UpdatePasswordReqBean requestBean) throws AuthenticationException, IOException {
		
		String userName = jwtTokenUtil.getUsernameFromToken(request.getHeader(tokenHeader));
		
		UsernamePasswordAuthenticationToken tokenAuth = new UsernamePasswordAuthenticationToken(
				userName, requestBean.getOldPassword());

		final Authentication authentication = authManager.authenticate(tokenAuth);
		
		if (!authentication.isAuthenticated()) {
			throw new BadCredentialsException();
		}
		
        UserEnt userEnt = userService.changePassword(userName , requestBean.getNewPassword());
		
		eventPublisher.publishEvent(new CustomApplicationEvent(userEnt, request.getLocale()));
		
		return ResponseEntity.ok(updatePasswordRespBean);
	}
	
	@PostMapping(value = "/changeProfilePhoto")
	public ResponseEntity<ChangeProfilePhotoRespBean> changeProfilePhoto(HttpServletRequest request , @RequestBody ChangeProfilePhotoReqBean requestBean) throws IOException {
		
		String username = jwtTokenUtil.getUsernameFromToken(request.getHeader(tokenHeader).substring(7));
		
		userService.changeProfilePhoto(username , requestBean.getPhoto());
		
		return ResponseEntity.ok(changeProfilePhoto);
	}
	
	@GetMapping(value = "/getInfoAfterLogin")
	public ResponseEntity<GetInfoAfterLoginRespBean> getInfoAfterLogin(HttpServletRequest request , GetInfoAfterLoginReqBean requestBean) throws IOException {
		
		String username = jwtTokenUtil.getUsernameFromToken(request.getHeader(tokenHeader));
		
		UserEnt userEnt = Optional.ofNullable(userService.findUserByUserName(username)).orElseThrow(UserNotFoundException::new);
		
		AfterLoginBean user =new AfterLoginBean();
		user.setMiddleName(userEnt.getUsername());
		user.setPhoto(userEnt.getPhoto());
		user.setUserName(userEnt.getUsername());
		getInfoAfterLogin.setUser(user);
		return ResponseEntity.ok(getInfoAfterLogin);
	}
	
	@PostMapping(value = "/addParent")
	public ResponseEntity<AddParentRespBean> addParent(@RequestBody AddParentReqBean requestBean) throws AuthenticationException, IOException {
		
		parentService.addOrUpdateParent (requestBean);
		
		return ResponseEntity.ok(addParentRespBean);
	}
	
	
	
	
	@PostMapping(value = "/ok")
	@PreAuthorize("hasRole('USER')")
	public String registerUser()  {
		

		return "hello world";
	}
}
