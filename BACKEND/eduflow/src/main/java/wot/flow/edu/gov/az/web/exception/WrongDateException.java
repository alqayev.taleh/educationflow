/**
 * 
 */
package wot.flow.edu.gov.az.web.exception;

/**
 * @author Alqayev Taleh
 *
 * 2017 Nov 10, 2017 11:43:58 PM
 */
public class WrongDateException  extends RuntimeException{

	private static final long serialVersionUID = 1363312971559580524L;
	
	public WrongDateException() {
        super();
    }

    public WrongDateException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public WrongDateException(final String message) {
        super(message);
    }

    public WrongDateException(final Throwable cause) {
        super(cause);
    }	
}
