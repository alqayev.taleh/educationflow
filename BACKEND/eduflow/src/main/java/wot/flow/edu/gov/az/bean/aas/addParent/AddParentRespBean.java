/**
 * 
 */
package wot.flow.edu.gov.az.bean.aas.addParent;

import wot.flow.edu.gov.az.utilities.bean.Response;
import wot.flow.edu.gov.az.utilities.enums.ErrorHandl;

/**
 * @author Alqayev Taleh
 *
 * 2017 Nov 18, 2017 9:43:03 PM
 */
public class AddParentRespBean extends Response {

	public AddParentRespBean(ErrorHandl coreMessage){
		super ( coreMessage.getErrorCode() , coreMessage.toString());
	}
	
}
