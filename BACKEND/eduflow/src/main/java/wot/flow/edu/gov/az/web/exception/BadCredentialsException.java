/**
 * 
 */
package wot.flow.edu.gov.az.web.exception;

/**
 * @author Alqayev Taleh
 *
 * 2017 Nov 18, 2017 6:38:01 PM
 */
public class BadCredentialsException extends RuntimeException {

	private static final long serialVersionUID = 1363312971559580524L;
	
	public BadCredentialsException() {
        super();
    }

    public BadCredentialsException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public BadCredentialsException(final String message) {
        super(message);
    }

    public BadCredentialsException(final Throwable cause) {
        super(cause);
    }	
}