/**
 * 
 */
package wot.flow.edu.gov.az.config.beans;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @author Alqayev Taleh
 *
 * 2017 Nov 10, 2017 12:06:38 PM
 */
@Component
@ConfigurationProperties(prefix = "datasource.sampleapp.postgres")
public class DataSourceType {
//	 @Value("${hibernate.dialect}") 
	private String dialect;

	public String getDialect() {
		return dialect;
	}

	public void setDialect(String dialect) {
		this.dialect = dialect;
	}
	
}
