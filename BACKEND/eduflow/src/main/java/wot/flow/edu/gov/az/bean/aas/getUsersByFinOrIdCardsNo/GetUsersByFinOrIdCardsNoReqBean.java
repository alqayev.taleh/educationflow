/**
 * 
 */
package wot.flow.edu.gov.az.bean.aas.getUsersByFinOrIdCardsNo;

import wot.flow.edu.gov.az.utilities.bean.Request;

/**
 * @author Alqayev Taleh
 *
 * 2017 Nov 18, 2017 8:14:56 PM
 */
public class GetUsersByFinOrIdCardsNoReqBean extends Request{
    
	private String finOrIdCardsNo;

	public String getFinOrIdCardsNo() {
		return finOrIdCardsNo;
	}

	public void setFinOrIdCardsNo(String finOrIdCardsNo) {
		this.finOrIdCardsNo = finOrIdCardsNo;
	}
    
    
}
