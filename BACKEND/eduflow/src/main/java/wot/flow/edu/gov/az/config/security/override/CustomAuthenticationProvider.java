/**
 * 
 */
package wot.flow.edu.gov.az.config.security.override;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Component;

import wot.flow.edu.gov.az.config.security.jwt.JwtUser;
import wot.flow.edu.gov.az.persistence.service.CustomUserDetailsService;
import wot.flow.edu.gov.az.web.exception.BadCredentialsException;

/**
 * @author Alqayev Taleh
 *
 * 2017 Nov 18, 2017 7:36:36 PM
 */
@Component
public class CustomAuthenticationProvider implements AuthenticationProvider{
 
    @Autowired
    private CustomUserDetailsService userService;
    
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
          String username = authentication.getName();
          String password = (String) authentication.getCredentials();
     
            JwtUser user = userService.loadUserByUsername(username);
     
            if (user == null || !user.getUsername().equalsIgnoreCase(username)) {
                throw new BadCredentialsException("Username not found.");
            }
     
            if (!password.equals(user.getPassword())) {
                throw new BadCredentialsException("Wrong password.");
            }
     
            Collection<? extends GrantedAuthority> authorities = user.getAuthorities();
     
            return new UsernamePasswordAuthenticationToken(user, password, authorities);
    }
 
    public boolean supports(Class<?> arg0) {
        return true;
    }
}
