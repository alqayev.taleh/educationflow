package wot.flow.edu.gov.az.persistence.entities.admin;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(schema = "admin" , name = "adm_language")
public class LanguageEnt {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private long id;
	
	@Column(name = "lang_val")
	private String val;
	
	@Column(name = "order_by")
	private Long orderBy;

	public Long getId() {
		return id;
	}

	public void setOid(Long id) {
		this.id = id;
	}

	public String getVal() {
		return val;
	}

	public void setVal(String val) {
		this.val = val;
	}

	public Long getOrderBy() {
		return orderBy;
	}

	public void setOrderBy(Long orderBy) {
		this.orderBy = orderBy;
	}

	@Override
	public String toString() {
		return "Language [oid=" + id + ", val=" + val + ", orderBy=" + orderBy + "]";
	}
}
