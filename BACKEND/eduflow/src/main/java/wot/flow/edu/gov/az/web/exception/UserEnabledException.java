/**
 * 
 */
package wot.flow.edu.gov.az.web.exception;

/**
 * @author Alqayev Taleh
 *
 * 2017 Nov 9, 2017 11:08:29 PM
 */
public class UserEnabledException  extends RuntimeException {

	private static final long serialVersionUID = 1363312971559580524L;
	
	public UserEnabledException() {
        super();
    }

    public UserEnabledException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public UserEnabledException(final String message) {
        super(message);
    }

    public UserEnabledException(final Throwable cause) {
        super(cause);
    }	
}