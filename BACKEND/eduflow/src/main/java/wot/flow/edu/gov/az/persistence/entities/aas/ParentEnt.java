/**
 * 
 */
package wot.flow.edu.gov.az.persistence.entities.aas;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import wot.flow.edu.gov.az.persistence.entities.BaseEnt;
import wot.flow.edu.gov.az.persistence.entities.admin.DictionaryDetailEnt;

/**
 * @author Alqayev Taleh
 *
 * 2017 Nov 18, 2017 9:18:20 PM
 */
@Entity
@Table(schema = "aas" , name = "aas_parents")
public class ParentEnt extends BaseEnt implements Serializable{
	
	private static final long serialVersionUID = 123423L;
	
	@ManyToOne
	@JoinColumn(name = "parent_id")
	private PersonEnt parent;
	
	@ManyToOne
	@JoinColumn(name = "child_id")
	private PersonEnt child;
	
	@Column(name = "email")
	private String email;
	
	@Column(name = "mobile")
	private String mobile;
	
	@Column(name = "home")
	private String home;
	
	@ManyToOne
	@JoinColumn(name = "parent_type")
	private DictionaryDetailEnt parentType;
	
	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "aas_parent_to_notification" , joinColumns = {@JoinColumn(name = "parent_id")} ,
	inverseJoinColumns = {@JoinColumn(name = "notification_id")})
	private Set<DictionaryDetailEnt> parentToNotifications = new HashSet<>();
	
	

	public Set<DictionaryDetailEnt> getParentToNotifications() {
		return parentToNotifications;
	}

	public void setParentToNotifications(Set<DictionaryDetailEnt> parentToNotifications) {
		this.parentToNotifications = parentToNotifications;
	}

	public PersonEnt getParent() {
		return parent;
	}

	public void setParent(PersonEnt parent) {
		this.parent = parent;
	}

	public PersonEnt getChild() {
		return child;
	}

	public void setChild(PersonEnt child) {
		this.child = child;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getHome() {
		return home;
	}

	public void setHome(String home) {
		this.home = home;
	}

	public DictionaryDetailEnt getParentType() {
		return parentType;
	}

	public void setParentType(DictionaryDetailEnt parentType) {
		this.parentType = parentType;
	}
}
