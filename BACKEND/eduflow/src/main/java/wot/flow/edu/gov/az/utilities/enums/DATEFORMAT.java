/**
 * 
 */
package wot.flow.edu.gov.az.utilities.enums;

/**
 * @author Alqayev Taleh
 *
 * 2017 Nov 10, 2017 11:20:06 PM
 */
public enum DATEFORMAT {
	DATABASE_DATE("yyyyMMdd"),
	DATABASE_TIME("HHmm"),
	DATABASE_DATE_TIME("yyyyMMddHHmmss"),
	DATABASE_DATE_TIME_WITHOUT_SECOND("yyyyMMddHHmm"),
	FRONT_DATE_TIME("dd-MM-yyyy HH:mm"),
	FRONT_DATE("dd-MM-yyyy"),
	IN_FRONT_DATE("yyyy-MM-dd"),
	FRONT_TIME("HH:mm"),
	FULL_DATABASE_DATE("yyyyMMddHHmmssSSS");
	


	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	private String date;

	private DATEFORMAT(String evalType) {
		this.date = evalType;
	}
}
