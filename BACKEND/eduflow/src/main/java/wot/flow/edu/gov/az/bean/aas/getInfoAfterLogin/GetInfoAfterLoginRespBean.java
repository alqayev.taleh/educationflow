/**
 * 
 */
package wot.flow.edu.gov.az.bean.aas.getInfoAfterLogin;

import wot.flow.edu.gov.az.utilities.bean.Response;
import wot.flow.edu.gov.az.utilities.enums.ErrorHandl;

/**
 * @author Alqayev Taleh
 *
 * 2017 Nov 25, 2017 5:29:29 PM
 */
public class GetInfoAfterLoginRespBean extends Response {

	public GetInfoAfterLoginRespBean(ErrorHandl coreMessage){
		super(coreMessage.getErrorCode() , coreMessage.toString());
	}

	private AfterLoginBean user;

	public AfterLoginBean getUser() {
		return user;
	}

	public void setUser(AfterLoginBean user) {
		this.user = user;
	}
}