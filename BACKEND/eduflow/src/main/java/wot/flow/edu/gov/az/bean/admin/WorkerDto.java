/**
 * 
 */
package wot.flow.edu.gov.az.bean.admin;

/**
 * @author Alqayev Taleh
 *
 * 2017 Nov 19, 2017 11:05:46 PM
 */
public class WorkerDto {

	private Long id;

	public WorkerDto(Long id) {
		super();
		this.id = id;
	}

	public Long getId() {
		return id;
	}	
}
