/**
 * 
 */
package wot.flow.edu.gov.az.bean.admin;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import wot.flow.edu.gov.az.bean.admin.getProfileDataByAdmin.GetProfileDataByAdminRespBean;
import wot.flow.edu.gov.az.utilities.enums.ErrorHandl;

/**
 * @author Alqayev Taleh
 *
 * 2017 Nov 19, 2017 11:23:31 PM
 */
@Configuration
public class BeanByAdmin {
	
		@Bean
	    public GetProfileDataByAdminRespBean succesRegisterOrUpdatePersonRespBean () {
	    	return new GetProfileDataByAdminRespBean(ErrorHandl.SUCCESS);
	    }
		
}
