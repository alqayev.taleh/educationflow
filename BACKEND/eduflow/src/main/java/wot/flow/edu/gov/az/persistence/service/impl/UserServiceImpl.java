/**
 * 
 */
package wot.flow.edu.gov.az.persistence.service.impl;

import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import wot.flow.edu.gov.az.bean.aas.NotificationDto;
import wot.flow.edu.gov.az.bean.aas.RoleDto;
import wot.flow.edu.gov.az.bean.aas.registerOrUpdateUser.RegisterOrUpdateUserReqBean;
import wot.flow.edu.gov.az.bean.aas.registerOrUpdateUser.UserBean;
import wot.flow.edu.gov.az.persistence.dao.DictionaryDetailRepository;
import wot.flow.edu.gov.az.persistence.dao.PersonRepository;
import wot.flow.edu.gov.az.persistence.dao.RoleRepository;
import wot.flow.edu.gov.az.persistence.dao.UserRepository;
import wot.flow.edu.gov.az.persistence.entities.aas.PersonEnt;
import wot.flow.edu.gov.az.persistence.entities.aas.RoleEnt;
import wot.flow.edu.gov.az.persistence.entities.aas.UserEnt;
import wot.flow.edu.gov.az.persistence.entities.admin.DictionaryDetailEnt;
import wot.flow.edu.gov.az.persistence.service.UserService;
import wot.flow.edu.gov.az.utilities.OidGenerator;
import wot.flow.edu.gov.az.utilities.Servant;
import wot.flow.edu.gov.az.utilities.enums.UTILENUMS;
import wot.flow.edu.gov.az.web.exception.EmailOrUsernameAlreadyExistException;
import wot.flow.edu.gov.az.web.exception.PersonNotFoundException;
import wot.flow.edu.gov.az.web.exception.PersonTypeAlreadyExistException;
import wot.flow.edu.gov.az.web.exception.RoleNotFoundException;
import wot.flow.edu.gov.az.web.exception.UserNotFoundException;

/**
 * @author Taleh-PC
 * 2017 Nov 4, 2017 2:27:22 AM
 */
@Service(value = "userService")
@Transactional
public class UserServiceImpl implements UserService{

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private PersonRepository personRepository;

	@Autowired
	private RoleRepository roleRepository;

	@Autowired
	private DictionaryDetailRepository dictionaryDetailRepository;

	@Autowired 
	private PasswordEncoder encoder;

	//	@Autowired 
	//	private UserToNotificationRepository userToNotificationRepository;

	//    @Autowired
	//    private EntBeans entBeans;

	@Override
	public UserEnt registerNewUserAccount(final RegisterOrUpdateUserReqBean requestBean) {

		UserBean user = requestBean.getUser();
		PersonEnt personEnt = personRepository.findByIdAndState(requestBean.getPersonOid(), UTILENUMS.ACTIVE.getValue());
		Optional.ofNullable(personEnt).orElseThrow(PersonNotFoundException::new);
		// @formatter:off
		//eger email bu persondan bashqasinda olarsa
		if (!userRepository.isRegisterUser(true, true, personEnt, user.getEmail(),user.getUsername()).isEmpty()) 
			throw new EmailOrUsernameAlreadyExistException();

		String username = Objects.nonNull(user.getUsername()) && !user.getUsername().trim().isEmpty() ?user.getUsername() : OidGenerator.generateOid();
		String password = Objects.nonNull(user.getPassword()) && !user.getPassword().trim().isEmpty() ?user.getPassword() : OidGenerator.generateOid();
		UserEnt userEnt = new UserEnt();
		userEnt.setAccountNonLocked(true);
		userEnt.setAuthId(requestBean.getAuthId());
		userEnt.setCreateDate(Servant.createdDate());
		userEnt.setEmail(user.getEmail());
		userEnt.setEnabled(true);
		userEnt.setPassword(encoder.encode(password));
		userEnt.setPerson(personEnt);
		userEnt.setState(UTILENUMS.ACTIVE.getValue());
		userEnt.setUsername(username);

		DictionaryDetailEnt personType = dictionaryDetailRepository.findByIdAndState(user.getPersonType(),UTILENUMS.ACTIVE.getValue());

		if (!userRepository.findByPersonTypeAndPerson(personType, personEnt).isEmpty())
			throw new PersonTypeAlreadyExistException();

		userEnt.setPersonType(personType);

		if(Objects.nonNull(user.getRoles())){
			Set<RoleEnt> roles = roleRepository.findByIdIn(user.getRoles());
			if(Objects.nonNull(roles))
				userEnt.setRoles(roles);
			else throw new RoleNotFoundException();
		}

		if(Objects.nonNull(user.getNotifications())) {
			Set<DictionaryDetailEnt> notifications = dictionaryDetailRepository.findByIdIn(user.getNotifications());
			//			if(Objects.nonNull(notifications))
			//				notifications.forEach(notification ->{
			////					UserToNotificationEnt userToNotification = new UserToNotificationEnt();
			////					userToNotification.setAuthId(requestBean.getAuthId());
			////					userToNotification.setCreateDate(Servant.createdDate());
			////					userToNotification.setState(UTILENUMS.ACTIVE.getValue());
			////					userToNotification.setNotification(notification);
			////					userToNotification.setUser(userEnt);
			//					userToNotificationRepository.save(userToNotification);
			//				});
			//			else throw new NotificationNotFoundException();
			userEnt.setUserNotifications(notifications);
		}
		UserEnt registered = userRepository.saveAndFlush(userEnt);
		registered.setPassword(password);
		return registered;
		// @formatter:on
	}

	/* (non-Javadoc)
	 * @see wot.flow.edu.gov.az.persistence.service.UserService#findUserByUserId(java.lang.Long)
	 */
	@Override
	public UserEnt findUserByUserId(Long id) {
		return Optional.ofNullable(userRepository.findOne(id)).orElseThrow(UserNotFoundException::new);
	}

	/* (non-Javadoc)
	 * @see wot.flow.edu.gov.az.persistence.service.UserService#changeRole(java.lang.Long, java.util.Set)
	 */
	@Override
	public void changeRole(Long id, Set<RoleDto> newRoles) {

		UserEnt user =  userRepository.findById(id);

		Set<RoleEnt> userRoles = user.getRoles();

		Set<RoleEnt> addRoles = roleRepository.findByIdIn(
				newRoles.stream()
				.map(RoleDto::getRoleId)
				.collect(Collectors.toSet()));

		Set<RoleEnt> removeRoles = null;
		if (Optional.ofNullable(userRoles).isPresent());
		removeRoles = userRoles.stream()
				.filter(role -> addRoles.add(role))
				.collect(Collectors.toSet());

		Set<RoleEnt> allRoles = new HashSet<>(userRoles);
		allRoles.addAll(addRoles);

		user.setRoles(allRoles);
		removeRoles.forEach(r ->{
			user.getRoles().remove(r);
		});

		userRepository.save(user);
	}

	/* (non-Javadoc)
	 * @see wot.flow.edu.gov.az.persistence.service.UserService#changeNotification(java.lang.Long, java.util.Set)
	 */
	@Override
	public void changeNotification(Long id, Set<NotificationDto> notifications) {
		UserEnt user =  userRepository.findById(id);

		Set<DictionaryDetailEnt> userNotifications = user.getUserNotifications();

		Set<DictionaryDetailEnt> addNotifications = dictionaryDetailRepository.findByIdIn(
				notifications.stream()
				.map(NotificationDto::getOid)
				.collect(Collectors.toSet()));

		Set<DictionaryDetailEnt> removeNotifications = null;
		if (Optional.ofNullable(userNotifications).isPresent());
		removeNotifications = userNotifications.stream()
				.filter(notification -> addNotifications.add(notification))
				.collect(Collectors.toSet());

		Set<DictionaryDetailEnt> allNotifications = new HashSet<>(userNotifications);
		allNotifications.addAll(addNotifications);

		user.setUserNotifications(allNotifications);
		removeNotifications.forEach(u ->{
			user.getUserNotifications().remove(u);
		});

		userRepository.save(user);
	}

	/* (non-Javadoc)
	 * @see wot.flow.edu.gov.az.persistence.service.UserService#changePassword(java.lang.Long, java.lang.String)
	 */
	@Override
	public UserEnt changePassword(String username, String newPassword) {
		String password = Objects.nonNull(newPassword) && !newPassword.trim().isEmpty() ? newPassword: OidGenerator.generateOid();
		UserEnt ent = userRepository.findByUsername(username);
		ent.setPassword(newPassword);
		ent = userRepository.saveAndFlush(ent);
		ent.setPassword(password);
		return ent;
	}

	/* (non-Javadoc)
	 * @see wot.flow.edu.gov.az.persistence.service.UserService#findUsersByFinOrIdCardsNo(java.lang.String)
	 */
	@Override
	public List<UserEnt> findUsersByFinOrIdCardsNo(String finOrIdCardsNo) {
		List<UserEnt> users = userRepository.findUsersByFinOrIdCardsNo(finOrIdCardsNo);
		return users;
	}

	/* (non-Javadoc)
	 * @see wot.flow.edu.gov.az.persistence.service.UserService#findUserByUserName(java.lang.String)
	 */
	@Override
	public UserEnt findUserByUserName(String username) {
		return userRepository.findByUsername(username);
	}

	/* (non-Javadoc)
	 * @see wot.flow.edu.gov.az.persistence.service.UserService#changeProfilePhoto(java.lang.String)
	 */
	@Override
	public void changeProfilePhoto(String username , String photo) {
		UserEnt userEnt = userRepository.findByUsername(username);
		Optional.ofNullable(userEnt).orElseThrow(UserNotFoundException::new);
		
		userEnt.setPhoto(photo);
		
		userRepository.save(userEnt);
	}
}
