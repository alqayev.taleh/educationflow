/**
 * 
 */
package wot.flow.edu.gov.az.persistence.service;

import java.util.Set;

import wot.flow.edu.gov.az.bean.aas.NotificationDto;
import wot.flow.edu.gov.az.bean.aas.addParent.AddParentReqBean;

/**
 * @author Alqayev Taleh
 *
 * 2017 Nov 18, 2017 10:06:27 PM
 */
public interface ParentService {

	/**
	 * @param requestBean
	 */
	void addOrUpdateParent(AddParentReqBean requestBean);

	/**
	 * @param id
	 * @param orElseThrow
	 */
	void changeNotification(Long id, Set<NotificationDto> newNotifications);

}
