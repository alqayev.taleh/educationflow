/**
 * 
 */
package wot.flow.edu.gov.az.web.exception;

/**
 * @author Alqayev Taleh
 *
 * 2017 Nov 13, 2017 1:55:30 AM
 */
public class PersonTypeAlreadyExistException extends RuntimeException{

	private static final long serialVersionUID = 1363312971559580524L;
	
	public PersonTypeAlreadyExistException() {
        super();
    }

    public PersonTypeAlreadyExistException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public PersonTypeAlreadyExistException(final String message) {
        super(message);
    }

    public PersonTypeAlreadyExistException(final Throwable cause) {
        super(cause);
    }	
}
