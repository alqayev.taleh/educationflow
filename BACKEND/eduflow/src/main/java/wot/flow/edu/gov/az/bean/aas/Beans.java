/**
 * 
 */
package wot.flow.edu.gov.az.bean.aas;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import wot.flow.edu.gov.az.bean.aas.addParent.AddParentRespBean;
import wot.flow.edu.gov.az.bean.aas.changeProfilePhoto.ChangeProfilePhotoRespBean;
import wot.flow.edu.gov.az.bean.aas.changeUserNotification.ChangeUserNotificationRespBean;
import wot.flow.edu.gov.az.bean.aas.changeUserRole.ChangeUserRoleRespBean;
import wot.flow.edu.gov.az.bean.aas.getInfoAfterLogin.GetInfoAfterLoginRespBean;
import wot.flow.edu.gov.az.bean.aas.getPersonByFin.GetPersonByFinRespBean;
import wot.flow.edu.gov.az.bean.aas.getPersons.GetPersonsRespBean;
import wot.flow.edu.gov.az.bean.aas.getUserByUserId.GetUserByUserIdRespBean;
import wot.flow.edu.gov.az.bean.aas.getUsersByFinOrIdCardsNo.GetUsersByFinOrIdCardsNoRespBean;
import wot.flow.edu.gov.az.bean.aas.registerOrUpdatePerson.RegisterOrUpdatePersonRespBean;
import wot.flow.edu.gov.az.bean.aas.registerOrUpdateUser.RegisterOrUpdateUserRespBean;
import wot.flow.edu.gov.az.bean.aas.updatePassword.UpdatePasswordRespBean;
import wot.flow.edu.gov.az.utilities.enums.ErrorHandl;

/**
 * @author Alqayev Taleh
 *
 * 2017 Nov 11, 2017 3:16:13 PM
 */
@Configuration
public class Beans {
	@Bean
    public RegisterOrUpdatePersonRespBean succesRegisterOrUpdatePersonRespBean () {
    	return new RegisterOrUpdatePersonRespBean(ErrorHandl.SUCCESS);
    }
	
	@Bean
    public RegisterOrUpdateUserRespBean succesRegisterOrUpdateUserRespBean () {
    	return new RegisterOrUpdateUserRespBean(ErrorHandl.SUCCESS);
    }
	
	@Bean
    public UpdatePasswordRespBean updatePasswordRespBean () {
    	return new UpdatePasswordRespBean(ErrorHandl.SUCCESS);
    }
	
	@Bean
    public GetPersonsRespBean getPersonsRespBean () {
    	return new GetPersonsRespBean(ErrorHandl.SUCCESS);
    }
	
	@Bean
    public GetPersonByFinRespBean getPersonByFinRespBean () {
    	return new GetPersonByFinRespBean(ErrorHandl.SUCCESS);
    }
	
	@Bean
    public GetUserByUserIdRespBean getUserByUserIdRespBean () {
    	return new GetUserByUserIdRespBean(ErrorHandl.SUCCESS);
    }
	
	@Bean
    public ChangeUserRoleRespBean changeUserRole () {
    	return new ChangeUserRoleRespBean(ErrorHandl.SUCCESS);
    }
	
	@Bean
    public ChangeUserNotificationRespBean changeUserNotificationRespBean () {
    	return new ChangeUserNotificationRespBean(ErrorHandl.SUCCESS);
    }
	
	@Bean
    public GetUsersByFinOrIdCardsNoRespBean getUsersByFinOrIdCardsNoRespBean () {
    	return new GetUsersByFinOrIdCardsNoRespBean(ErrorHandl.SUCCESS);
    }
	
	@Bean
    public AddParentRespBean addParentRespBean () {
    	return new AddParentRespBean(ErrorHandl.SUCCESS);
    }
	
	@Bean
    public RegisterOrUpdatePersonRespBean registerOrUpdatePersonRespBean () {
    	return new RegisterOrUpdatePersonRespBean(ErrorHandl.SUCCESS);
    }
	
	@Bean
    public ChangeProfilePhotoRespBean changeProfilePhoto () {
    	return new ChangeProfilePhotoRespBean(ErrorHandl.SUCCESS);
    }
	
	@Bean
    public GetInfoAfterLoginRespBean getInfoAfterLogin () {
    	return new GetInfoAfterLoginRespBean(ErrorHandl.SUCCESS);
    }
	
	
	
}
