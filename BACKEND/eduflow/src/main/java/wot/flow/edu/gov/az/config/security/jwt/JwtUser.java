///**
// * 
// */
//package wot.flow.edu.gov.az.config.security.jwt;
//
//import java.io.Serializable;
//import java.util.Collection;
//import java.util.Date;
//import java.util.stream.Collectors;
//
//import org.springframework.security.core.GrantedAuthority;
//import org.springframework.security.core.userdetails.UserDetails;
//
//import com.fasterxml.jackson.annotation.JsonIgnore;
//
//import wot.flow.edu.gov.az.bean.aas.NotificationDto;
//import wot.flow.edu.gov.az.persistence.entities.aas.UserEnt;
//
///**
// * @author Taleh-PC
// * 2017 Nov 4, 2017 2:10:37 PM
// */
//public class JwtUser implements UserDetails, Serializable {
//
//	private static final long serialVersionUID = -756497092925944731L;
//
//	private final JwtPerson person;
//	private final Long uOid;
//	private final String email;
//	private final String username;
//	private final Collection<? extends GrantedAuthority> authorities;
//	private final Collection<? extends NotificationDto> notifications;
//	private final boolean accountNonExpired;
//	private final boolean accountNonLocked;
//	private final boolean enabled;
//	private final String password;
//	private final String lastPasswordResetDate;
//
//	public JwtUser(UserEnt ent) {
//		this.person = new JwtPerson(
//				ent.getPerson().getId(), 
//				ent.getPerson().getName(), 
//				ent.getPerson().getSurname(), 
//				ent.getPerson().getMiddlename(), 
//				ent.getPerson().getCitizenship(), 
//				ent.getPerson().getFin(), 
//				ent.getPerson().getIdCardsNo(), 
//				ent.getPerson().getGender(), 
//				ent.getPerson().getBirthday());
//		this.uOid = ent.getId();
//		this.email = ent.getEmail();
//		this.username = ent.getUsername();
//		this.authorities = ent.getAuthorities();
//		this.notifications = ent.getUserNotifications().stream()
//				.map(notification -> new NotificationDto(notification.getId(),notification.getName()))
//				.collect(Collectors.toList());
//		this.accountNonExpired = ent.isAccountNonExpired();
//		this.accountNonLocked = ent.isAccountNonLocked();
//		this.enabled = ent.isEnabled();
//		this.password = ent.getPassword();
//		this.lastPasswordResetDate = ent.getLastPasswordResetDate();
//		
//	}
//	
//	
//
//	public String getLastPasswordResetDate() {
//		return lastPasswordResetDate;
//	}
//
//
//
//	public JwtPerson getPerson() {
//		return person;
//	}
//
//
//
//	public Long getuOid() {
//		return uOid;
//	}
//
//
//
//	public String getEmail() {
//		return email;
//	}
//
//
//
//	public String getUsername() {
//		return username;
//	}
//
//
//
//	public Collection<? extends NotificationDto> getNotifications() {
//		return notifications;
//	}
//
//
//
//	@JsonIgnore
//	@Override
//	public boolean isAccountNonExpired() {
//		return accountNonExpired;
//	}
//
//	@JsonIgnore
//	@Override
//	public boolean isAccountNonLocked() {
//		return accountNonLocked;
//	}
//
//	@JsonIgnore
//	@Override
//	public boolean isCredentialsNonExpired() {
//		return true;
//	}
//
//	@Override
//	public Collection<? extends GrantedAuthority> getAuthorities() {
//		return authorities;
//	}
//
//	@Override
//	public boolean isEnabled() {
//		return enabled;
//	}
//
//	@Override
//	public int hashCode() {
//		final int prime = 31;
//		int result = 1;
//		result = (prime * result) + ((email == null) ? 0 : email.hashCode());
//		return result;
//	}
//
//	@Override
//	public boolean equals(final Object obj) {
//		if (this == obj) {
//			return true;
//		}
//		if (obj == null) {
//			return false;
//		}
//		if (getClass() != obj.getClass()) {
//			return false;
//		}
//		final JwtUser user = (JwtUser) obj;
//		if (!email.equals(user.email)) {
//			return false;
//		}
//		return true;
//	}
//
//	@Override
//	public String getPassword() {
//		return password;
//	}
//}

/**
 * 
 */
package wot.flow.edu.gov.az.config.security.jwt;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * @author Taleh-PC
 * 2017 Nov 4, 2017 2:10:37 PM
 */
public class JwtUser implements UserDetails, Serializable {

	private static final long serialVersionUID = -756497092925944731L;

	private final boolean accountNonLocked;
	private final boolean enabled;
	private final String password;
	private final String username;
	private String dbAuthorities;
	private Collection<String> chAuthorities;
	
	public JwtUser(boolean accountNonLocked
			, boolean enabled
			, String password
			, String username
			, String dbAuthorities) {
		super();
		this.accountNonLocked = accountNonLocked;
		this.enabled = enabled;
		this.password = password;
		this.username = username;
		this.dbAuthorities = dbAuthorities;
	}
	

	public JwtUser(boolean accountNonLocked, boolean enabled, String password, String username,
			Collection<String> chAuthorities) {
		super();
		this.accountNonLocked = accountNonLocked;
		this.enabled = enabled;
		this.password = password;
		this.username = username;
		this.chAuthorities = chAuthorities;
	}






	public Collection<String> getChAuthorities() {
		return chAuthorities;
	}






	public void setChAuthorities(Collection<String> chAuthorities) {
		this.chAuthorities = chAuthorities;
	}






	public String getDbAuthorities() {
		return dbAuthorities;
	}




	public void setDbAuthorities(String dbAuthorities) {
		this.dbAuthorities = dbAuthorities;
	}




	public String getUsername() {
		return username;
	}

	@JsonIgnore
	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@JsonIgnore
	@Override
	public boolean isAccountNonLocked() {
		return accountNonLocked;
	}

	@JsonIgnore
	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		return enabled;
	}
	
	private static List<GrantedAuthority> mapToGrantedAuthorities(Collection<String> authorities) {
		return authorities.stream().map(authority -> new SimpleGrantedAuthority(authority))
				.collect(Collectors.toList());
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return mapToGrantedAuthorities(getChAuthorities());
	}

	@Override
	public String getPassword() {
		return password;
	}
}

