/**
 * 
 */
package wot.flow.edu.gov.az.bean.admin;

/**
 * @author Alqayev Taleh
 *
 * 2017 Nov 19, 2017 11:03:06 PM
 */
public class PersonDtoByAdmin {
	
	private Long personId;

	private String birthday;

	private String citizenship;

	private String fin;

	private String gender;

	private String idCardsNo;

	private String middlename;

	private String name;

	private String surname;
	
	private UserDtoByAdmin user;

	public PersonDtoByAdmin(Long personId, String birthday, String citizenship, String fin, String gender, String idCardsNo,
			String middlename, String name, String surname,UserDtoByAdmin user) {
		super();
		this.personId = personId;
		this.birthday = birthday;
		this.citizenship = citizenship;
		this.fin = fin;
		this.gender = gender;
		this.idCardsNo = idCardsNo;
		this.middlename = middlename;
		this.name = name;
		this.surname = surname;
		this.user = user;
	}

	public UserDtoByAdmin getUser() {
		return user;
	}

	public Long getPersonId() {
		return personId;
	}

	public void setPersonId(Long personId) {
		this.personId = personId;
	}

	public String getBirthday() {
		return birthday;
	}

	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}

	public String getCitizenship() {
		return citizenship;
	}

	public void setCitizenship(String citizenship) {
		this.citizenship = citizenship;
	}

	public String getFin() {
		return fin;
	}

	public void setFin(String fin) {
		this.fin = fin;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getIdCardsNo() {
		return idCardsNo;
	}

	public void setIdCardsNo(String idCardsNo) {
		this.idCardsNo = idCardsNo;
	}

	public String getMiddlename() {
		return middlename;
	}

	public void setMiddlename(String middlename) {
		this.middlename = middlename;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}


}
