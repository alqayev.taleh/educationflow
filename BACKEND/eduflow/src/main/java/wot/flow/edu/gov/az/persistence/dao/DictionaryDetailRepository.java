/**
 * 
 */
package wot.flow.edu.gov.az.persistence.dao;

import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;

import wot.flow.edu.gov.az.persistence.entities.admin.DictionaryDetailEnt;

/**
 * @author Alqayev Taleh
 *
 * 2017 Nov 12, 2017 11:25:43 PM
 */
public interface DictionaryDetailRepository extends JpaRepository<DictionaryDetailEnt, Long> {
	Set<DictionaryDetailEnt> findByIdIn (Set<Long> ids);
	DictionaryDetailEnt findByIdAndState (Long id,Integer state);
}
