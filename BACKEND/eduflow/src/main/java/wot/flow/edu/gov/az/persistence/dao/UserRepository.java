package wot.flow.edu.gov.az.persistence.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import wot.flow.edu.gov.az.config.security.jwt.JwtUser;
import wot.flow.edu.gov.az.persistence.entities.aas.PersonEnt;
import wot.flow.edu.gov.az.persistence.entities.aas.UserEnt;
import wot.flow.edu.gov.az.persistence.entities.admin.DictionaryDetailEnt;


/**
 * @author Alqayev Taleh
 *
 * 2017 Nov 11, 2017 12:59:53 AM
 */
public interface UserRepository extends JpaRepository<UserEnt, Long> {
	
	
	UserEnt findByUsername(String username);
    
	@Query("SELECT"+
			" new wot.flow.edu.gov.az.config.security.jwt.JwtUser(u.accountNonLocked," +
			" u.enabled,"+
			" u.password,"+
			" u.username,"+
		    " roles.name)" +
		 " FROM "+
			" UserEnt AS u"+
		    " LEFT JOIN u.roles AS roles "+
		    " WHERE"+
			" u.username = :in_username")
			public List<JwtUser> findJwtUserByUsername(final @Param("in_username") String username);
	
    // @formatter:off
    @Query("SELECT 	u.username FROM 	UserEnt u WHERE "
    		+ "EXISTS "
    		+ "    ( SELECT 1 FROM UserEnt su WHERE "
    		+ "         su.accountNonLocked= :in_accountNonLocked "
    		+ "         AND su.enabled= :in_enabled "
    		+ "         AND su.person<> :in_personId "
    		+ "         AND su.email= :in_email) "
    		+ "OR (u.accountNonLocked= :in_accountNonLocked "
    		+ "AND u.enabled= :in_enabled "
    		+ "AND u.username = :in_username) ")
    // @formatter:on
    List<String> isRegisterUser(final @Param("in_accountNonLocked") Boolean in_accountNonLocked
    		,final @Param("in_enabled") Boolean in_enabled
    		,final @Param("in_personId") PersonEnt in_personId
    		,final @Param("in_email") String in_email
    		,final @Param("in_username") String in_username);
   
    UserEnt findById(final Long id);
    
    List<UserEnt> findByPersonTypeAndPerson(final DictionaryDetailEnt personType,final PersonEnt person);

	/**
	 * @param finOrIdCardsNo
	 * @return
	 */
    @Query("select u from UserEnt u inner join u.person p where p.fin = :in_finOrIdCardsNo or p.idCardsNo = :in_finOrIdCardsNo")
	List<UserEnt> findUsersByFinOrIdCardsNo(final @Param("in_finOrIdCardsNo") String in_finOrIdCardsNo);
}
