/**
 * 
 */
package wot.flow.edu.gov.az.bean.aas.getPersonByFin;

import wot.flow.edu.gov.az.bean.aas.PersonDto;
import wot.flow.edu.gov.az.utilities.bean.Response;
import wot.flow.edu.gov.az.utilities.enums.ErrorHandl;

/**
 * @author Alqayev Taleh
 *
 * 2017 Nov 15, 2017 9:24:11 PM
 */
public class GetPersonByFinRespBean extends Response {
   
	private PersonDto person;

	public PersonDto getPerson() {
		return person;
	}

	public void setPerson(PersonDto person) {
		this.person = person;
	}
	
	public GetPersonByFinRespBean(ErrorHandl coreMessage){
		super (coreMessage.getErrorCode(), coreMessage.toString());
	}
}
