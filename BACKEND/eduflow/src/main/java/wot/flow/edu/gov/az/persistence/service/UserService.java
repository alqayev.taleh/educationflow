/**
 * 
 */
package wot.flow.edu.gov.az.persistence.service;

import java.util.List;
import java.util.Set;

import wot.flow.edu.gov.az.bean.aas.NotificationDto;
import wot.flow.edu.gov.az.bean.aas.RoleDto;
import wot.flow.edu.gov.az.bean.aas.registerOrUpdateUser.RegisterOrUpdateUserReqBean;
import wot.flow.edu.gov.az.persistence.entities.aas.UserEnt;

/**
 * @author Taleh-PC
 * 2017 Nov 4, 2017 2:27:12 AM
 */
public interface UserService {

	/**
	 * @param requestBean
	 * @return
	 */
	 UserEnt registerNewUserAccount(final RegisterOrUpdateUserReqBean requestBean);

	/**
	 * @param id
	 * @return
	 */
	UserEnt findUserByUserId(Long id);

	/**
	 * @param id
	 * @param orElseThrow
	 */
	void changeRole(Long id, Set<RoleDto> newRoles);

	/**
	 * @param id
	 * @param orElseThrow
	 */
	void changeNotification(Long id, Set<NotificationDto> notifications);

	/**
	 * @param userId
	 * @param newPassword
	 * @return
	 */
	UserEnt changePassword(String username, String newPassword);

	/**
	 * @param finOrIdCardsNo
	 * @return
	 */
	List<UserEnt> findUsersByFinOrIdCardsNo(String finOrIdCardsNo);

	/**
	 * @param username
	 * @return
	 */
	UserEnt findUserByUserName(String username);

	/**
	 * @param username
	 */
	void changeProfilePhoto(String username , String photo);

}
