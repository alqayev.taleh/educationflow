/**
 * 
 */
package wot.flow.edu.gov.az.bean.aas.getUserByUserId;

import wot.flow.edu.gov.az.bean.aas.UserDto;
import wot.flow.edu.gov.az.utilities.bean.Response;
import wot.flow.edu.gov.az.utilities.enums.ErrorHandl;

/**
 * @author Alqayev Taleh
 *
 * 2017 Nov 15, 2017 10:29:28 PM
 */
public class GetUserByUserIdRespBean extends Response {
	private UserDto user;

	public UserDto getUser() {
		return user;
	}

	public void setUser(UserDto user) {
		this.user = user;
	}

	public GetUserByUserIdRespBean(ErrorHandl coreMessage){
		super ( coreMessage.getErrorCode() , coreMessage.toString());
	}
}
