/**
 * 
 */
package wot.flow.edu.gov.az.web.exception;

/**
 * @author Alqayev Taleh
 *
 * 2017 Nov 11, 2017 4:44:06 PM
 */
public class DuplicateUsernameOrPersonTypexception extends RuntimeException {

	private static final long serialVersionUID = 1363312971559580524L;
	
	public DuplicateUsernameOrPersonTypexception() {
        super();
    }

    public DuplicateUsernameOrPersonTypexception(final String message, final Throwable cause) {
        super(message, cause);
    }

    public DuplicateUsernameOrPersonTypexception(final String message) {
        super(message);
    }

    public DuplicateUsernameOrPersonTypexception(final Throwable cause) {
        super(cause);
    }	
}