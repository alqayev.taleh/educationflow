package wot.flow.edu.gov.az.persistence.dao;

import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;

import wot.flow.edu.gov.az.persistence.entities.aas.NotificationsEnt;

/**
 * Created by nydiarra on 10/05/17.
 */
public interface NotificationsRepository extends JpaRepository<NotificationsEnt, Long> {
    Set<NotificationsEnt> findByIdIn (Set<Long> notifications);
}
