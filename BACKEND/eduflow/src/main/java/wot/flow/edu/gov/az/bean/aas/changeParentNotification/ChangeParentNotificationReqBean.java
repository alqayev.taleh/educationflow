/**
 * 
 */
package wot.flow.edu.gov.az.bean.aas.changeParentNotification;

import java.util.Set;

import wot.flow.edu.gov.az.bean.aas.NotificationDto;
import wot.flow.edu.gov.az.utilities.bean.Request;

/**
 * @author Alqayev Taleh
 *
 * 2017 Nov 18, 2017 10:46:34 PM
 */
public class ChangeParentNotificationReqBean extends Request {
	private Long id;

	private Set<NotificationDto> notifications;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Set<NotificationDto> getNotifications() {
		return notifications;
	}

	public void setNotifications(Set<NotificationDto> notifications) {
		this.notifications = notifications;
	}
}
