/**
 * 
 */
package wot.flow.edu.gov.az.persistence.service.impl;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import wot.flow.edu.gov.az.bean.aas.registerOrUpdatePerson.PersonBean;
import wot.flow.edu.gov.az.bean.aas.registerOrUpdatePerson.RegisterOrUpdatePersonReqBean;
import wot.flow.edu.gov.az.config.beans.EntBeans;
import wot.flow.edu.gov.az.persistence.dao.PersonRepository;
import wot.flow.edu.gov.az.persistence.entities.aas.PersonEnt;
import wot.flow.edu.gov.az.persistence.service.PersonService;
import wot.flow.edu.gov.az.utilities.Servant;
import wot.flow.edu.gov.az.utilities.enums.DATEFORMAT;
import wot.flow.edu.gov.az.utilities.enums.UTILENUMS;
import wot.flow.edu.gov.az.web.exception.FillIdCardsNoOrFinException;
import wot.flow.edu.gov.az.web.exception.FinOrIdCardNoAlreadyExsistException;
import wot.flow.edu.gov.az.web.exception.PersonNotFoundException;

/**
 * @author Taleh-PC
 * 2017 Nov 4, 2017 2:18:52 PM
 */
@Service(value = "personService")
@Transactional
public class PersonServiceImpl implements PersonService {

	@Autowired 
	private PersonRepository personRepository;

	@Autowired
	@Qualifier(value = "person")
	private PersonEnt personEnt;

	@Autowired
	private EntBeans entBeans;

	public PersonEnt findByOidAndState (final Long id , final Integer state) {
		return personRepository.findByIdAndState(id, state);
	}

	/* (non-Javadoc)
	 * @see wot.flow.edu.gov.az.persistence.service.PersonService#registerNewPersonAccount(wot.flow.edu.gov.az.bean.aas.registerOrUpdate.PersonBean)
	 */
	@Override
	public PersonEnt registerNewPersonAccount(RegisterOrUpdatePersonReqBean reqBean) {
		PersonBean person = reqBean.getPerson();

		if (Objects.isNull(person.getFin()) && Objects.isNull(person.getIdCardsNo())) 
			throw  new FillIdCardsNoOrFinException();

		if (Objects.isNull(person.getPersonOid())) {
			if (Objects.nonNull(personRepository.findByFinOrIdCardsNo(person.getFin(), person.getIdCardsNo(),UTILENUMS.ACTIVE.getValue())))
				throw  new FinOrIdCardNoAlreadyExsistException();
			PersonEnt personEnt = entBeans.personEnt();
			personEnt.setAuthId(reqBean.getAuthId()); 
			personEnt.setBirthday(Servant.format(person.getBirthday(),DATEFORMAT.FRONT_DATE.getDate(),DATEFORMAT.DATABASE_DATE.getDate()));
			personEnt.setCitizenship(person.getCitizenship());
			personEnt.setCreateDate(Servant.createdDate());
			personEnt.setFin(person.getFin());
			personEnt.setGender(person.getGender());
			personEnt.setIdCardsNo(person.getIdCardsNo());
			personEnt.setMiddlename(person.getMiddlename());
			personEnt.setName(person.getName());
			personEnt.setState(UTILENUMS.ACTIVE.getValue());
			personEnt.setSurname(person.getSurname());
			return personRepository.saveAndFlush(personEnt);
		}
		if (Objects.nonNull(personRepository.findByFinOrIdCardsNo(person.getPersonOid(),person.getFin(), person.getIdCardsNo())))
			throw  new FinOrIdCardNoAlreadyExsistException();

		personEnt = Optional
				.ofNullable(personRepository.findByIdAndState(person.getPersonOid(), UTILENUMS.ACTIVE.getValue()))
				.orElseThrow(PersonNotFoundException::new);
		personEnt.setAuthId(reqBean.getAuthId()); 
		personEnt.setBirthday(Servant.format(person.getBirthday(),DATEFORMAT.FRONT_DATE.getDate(),DATEFORMAT.DATABASE_DATE.getDate()));
		personEnt.setCitizenship(person.getCitizenship());
		personEnt.setUpdateDate(Servant.createdDate());
		personEnt.setFin(person.getFin());
		personEnt.setGender(person.getGender());
		personEnt.setIdCardsNo(person.getIdCardsNo());
		personEnt.setMiddlename(person.getMiddlename());
		personEnt.setName(person.getName());
		personEnt.setSurname(person.getSurname());
		return personRepository.save(personEnt);
	}


	public List<PersonEnt> getPersonsBySate(Integer state,Integer page,Integer size){
		return personRepository.findAllByState(UTILENUMS.ACTIVE.getValue(),new PageRequest(page, size,Sort.Direction.ASC,"id"));
	}

	/* (non-Javadoc)
	 * @see wot.flow.edu.gov.az.persistence.service.PersonService#findPersonByFin(java.lang.String)
	 */
	@Override
	public PersonEnt findPersonByFinOrIdCardsNo(String fin , String idCardsNo) {
		return Optional.ofNullable(personRepository.findByFinOrIdCardsNo(fin, idCardsNo,UTILENUMS.ACTIVE.getValue())).orElseThrow(PersonNotFoundException::new);
	}
}
