/**
 * 
 */
package wot.flow.edu.gov.az.bean.aas.updatePassword;

import wot.flow.edu.gov.az.utilities.bean.Request;

/**
 * @author Alqayev Taleh
 *
 * 2017 Nov 12, 2017 2:10:02 PM
 */
public class UpdatePasswordReqBean extends Request {

	private String newPassword;
	
	private String oldPassword;
	
	private String retypePaswword;
	
	public String getNewPassword() {
		return newPassword;
	}
	
	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}
	
	public String getOldPassword() {
		return oldPassword;
	}
	
	public void setOldPassword(String oldPassword) {
		this.oldPassword = oldPassword;
	}
	
	public String getRetypePaswword() {
		return retypePaswword;
	}
	
	public void setRetypePaswword(String retypePaswword) {
		this.retypePaswword = retypePaswword;
	}
}
