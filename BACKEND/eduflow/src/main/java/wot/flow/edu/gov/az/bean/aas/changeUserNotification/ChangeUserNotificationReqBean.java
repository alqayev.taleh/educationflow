/**
 * 
 */
package wot.flow.edu.gov.az.bean.aas.changeUserNotification;

import java.util.Set;

import wot.flow.edu.gov.az.bean.aas.NotificationDto;

/**
 * @author Alqayev Taleh
 *
 * 2017 Nov 17, 2017 3:46:41 PM
 */
public class ChangeUserNotificationReqBean {
	
	private Long id;

	private Set<NotificationDto> notifications;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Set<NotificationDto> getNotifications() {
		return notifications;
	}

	public void setNotifications(Set<NotificationDto> notifications) {
		this.notifications = notifications;
	}
}
