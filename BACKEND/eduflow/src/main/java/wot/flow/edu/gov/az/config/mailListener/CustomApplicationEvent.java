package wot.flow.edu.gov.az.config.mailListener;

import java.util.Locale;

import org.springframework.context.ApplicationEvent;

import wot.flow.edu.gov.az.persistence.entities.aas.UserEnt;

@SuppressWarnings("serial")
public class CustomApplicationEvent extends ApplicationEvent{

	private final Locale locale;
	private final UserEnt userEnt;
	public CustomApplicationEvent( final UserEnt userEnt, final Locale locale) {
		super(userEnt);
		this.locale = locale;
		this.userEnt = userEnt;
	}

	public Locale getLocale() {
		return locale;
	}
	public UserEnt getUserEnt() {
		return userEnt;
	}

}
