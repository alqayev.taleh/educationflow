package wot.flow.edu.gov.az.web.error;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import wot.flow.edu.gov.az.utilities.bean.Response;
import wot.flow.edu.gov.az.utilities.enums.ErrorHandl;
import wot.flow.edu.gov.az.web.exception.BadCredentialsException;
import wot.flow.edu.gov.az.web.exception.DictionaryDetailNotFoundException;
import wot.flow.edu.gov.az.web.exception.DuplicateUsernameOrPersonTypexception;
import wot.flow.edu.gov.az.web.exception.EmailOrUsernameAlreadyExistException;
import wot.flow.edu.gov.az.web.exception.ExpiredJwtException;
import wot.flow.edu.gov.az.web.exception.FillIdCardsNoOrFinException;
import wot.flow.edu.gov.az.web.exception.FinOrIdCardNoAlreadyExsistException;
import wot.flow.edu.gov.az.web.exception.InvalidHeaderException;
import wot.flow.edu.gov.az.web.exception.InvalidJsonFormatException;
import wot.flow.edu.gov.az.web.exception.NotificationNotFoundException;
import wot.flow.edu.gov.az.web.exception.PersonNotFoundException;
import wot.flow.edu.gov.az.web.exception.PersonTypeAlreadyExistException;
import wot.flow.edu.gov.az.web.exception.RoleNotFoundException;
import wot.flow.edu.gov.az.web.exception.UserLockedException;
import wot.flow.edu.gov.az.web.exception.UserNotFoundException;
import wot.flow.edu.gov.az.web.exception.WrongDateException;

@ControllerAdvice
public class RestResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {
	
	@ExceptionHandler({ UserNotFoundException.class })
	public ResponseEntity<?> handleUserNotFound(final Exception ex, final WebRequest request) {
		ErrorHandl cm = ErrorHandl.USER_NOT_FOUND;
		return ResponseEntity.ok(new Response(cm));
	}
	
	@ExceptionHandler({ InvalidHeaderException.class })
	public ResponseEntity<Object> invalidHeader(final RuntimeException ex, final WebRequest request) {
		ErrorHandl cm = ErrorHandl.INVALID_HEADER;
		return ResponseEntity.ok(new Response(cm));
	}
	
	@ExceptionHandler({ ExpiredJwtException.class })
	public ResponseEntity<Object> expiredJwtException(final RuntimeException ex, final WebRequest request) {
		ErrorHandl cm = ErrorHandl.EXPIRED_JWT;
		return ResponseEntity.ok(new Response(cm));
	}
	
	@ExceptionHandler({ IllegalArgumentException.class })
	public ResponseEntity<Object> illegalArgumentException(final RuntimeException ex, final WebRequest request) {
		ErrorHandl cm = ErrorHandl.ILLEGAL_ARGUMENT_IN_JWT;
		return ResponseEntity.ok(new Response(cm));
	}
	
	@ExceptionHandler({ UserLockedException.class })
	public ResponseEntity<Object> accounLockException(final Exception ex, final WebRequest request) {
		ErrorHandl cm = ErrorHandl.ACCOUNT_LOCKED;
		return ResponseEntity.ok(new Response(cm));
	}
	
	@ExceptionHandler({ InvalidJsonFormatException.class })
	public ResponseEntity<Object> invaliJsonFormatException(final Exception ex, final WebRequest request) {
		ErrorHandl cm = ErrorHandl.INVALID_JSON_FORMAT;
		return ResponseEntity.ok(new Response(cm));
	}
	
	@ExceptionHandler({ WrongDateException.class })
	public ResponseEntity<Object> wrongDateException(final Exception ex, final WebRequest request) {
		ErrorHandl cm = ErrorHandl.WRON_DATE;
		return ResponseEntity.ok(new Response(cm));
	}
	
	@ExceptionHandler({ PersonNotFoundException.class })
	public ResponseEntity<Object> personNotFoundException(final Exception ex, final WebRequest request) {
		ErrorHandl cm = ErrorHandl.PERSON_NOT_FOUND;
		return ResponseEntity.ok(new Response(cm));
	}
	
	@ExceptionHandler({ FinOrIdCardNoAlreadyExsistException.class })
	public ResponseEntity<Object> finOrIdCardNoAlreadyExsistException(final Exception ex, final WebRequest request) {
		ErrorHandl cm = ErrorHandl.FIN_OR_IDCARDSNO_ALREADY_EXIST;
		return ResponseEntity.ok(new Response(cm));
	}
	
	@ExceptionHandler({ FillIdCardsNoOrFinException.class })
	public ResponseEntity<Object> fillIdCardsNoOrFinException(final Exception ex, final WebRequest request) {
		ErrorHandl cm = ErrorHandl.FILL_ID_CARDS_NO_OR_FIN;
		return ResponseEntity.ok(new Response(cm));
	}
	
	@ExceptionHandler({ DuplicateUsernameOrPersonTypexception.class })
	public ResponseEntity<Object> duplicateUsernameSamePersonException(final Exception ex, final WebRequest request) {
		ErrorHandl cm = ErrorHandl.DUPLICATE_USERNAME_OR_PERSON_TYPE;
		return ResponseEntity.ok(new Response(cm));
	}
	
	@ExceptionHandler({ EmailOrUsernameAlreadyExistException.class })
	public ResponseEntity<Object> emailAlreadyExistException(final Exception ex, final WebRequest request) {
		ErrorHandl cm = ErrorHandl.EMAIL_OR_USERNAME_ALREADY_EXIST;
		return ResponseEntity.ok(new Response(cm));
	}
	
	@ExceptionHandler({ NotificationNotFoundException.class })
	public ResponseEntity<Object> notificationNotFoundException(final Exception ex, final WebRequest request) {
		ErrorHandl cm = ErrorHandl.NOTIFICATION_NOT_FOUND;
		return ResponseEntity.ok(new Response(cm));
	}
	
	@ExceptionHandler({ RoleNotFoundException.class })
	public ResponseEntity<Object> roleNotFoundException(final Exception ex, final WebRequest request) {
		ErrorHandl cm = ErrorHandl.ROLE_NOT_FOUND;
		return ResponseEntity.ok(new Response(cm));
	}
	
	@ExceptionHandler({ PersonTypeAlreadyExistException.class })
	public ResponseEntity<Object> personTypeAlreadyExistException(final Exception ex, final WebRequest request) {
		ErrorHandl cm = ErrorHandl.PERSON_TYPE_ALREADY_EXIST;
		return ResponseEntity.ok(new Response(cm));
	}
	
	@ExceptionHandler({ BadCredentialsException.class })
	public ResponseEntity<Object> badCredentialsException(final Exception ex, final WebRequest request) {
		ErrorHandl cm = ErrorHandl.BADCREDENTIALS;
		return ResponseEntity.ok(new Response(cm));
	}
	
	@ExceptionHandler({ DictionaryDetailNotFoundException.class })
	public ResponseEntity<Object> dictionaryDetailNotFoundException(final Exception ex, final WebRequest request) {
		ErrorHandl cm = ErrorHandl.DICTIONARY_DETAIL_NOT_FOUND;
		return ResponseEntity.ok(new Response(cm));
	}
	
	
}
