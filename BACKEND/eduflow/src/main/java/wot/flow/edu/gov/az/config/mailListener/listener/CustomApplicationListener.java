package wot.flow.edu.gov.az.config.mailListener.listener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.core.env.Environment;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Component;

import wot.flow.edu.gov.az.config.mailListener.CustomApplicationEvent;
import wot.flow.edu.gov.az.persistence.entities.aas.UserEnt;

@Component
public class CustomApplicationListener implements ApplicationListener<CustomApplicationEvent> {
	
	@Autowired 
	JavaMailSender mailSender;
	
	@Autowired
    private Environment env;
	
	@Override
	public void onApplicationEvent(CustomApplicationEvent event) {
		this.confirmRegistration(event);
	}
	
	 private void confirmRegistration(final CustomApplicationEvent event) {
	        final UserEnt user = event.getUserEnt();
	        final SimpleMailMessage email = constructEmailMessage(event, user);
	        mailSender.send(email);
	    }
	 
	  private final SimpleMailMessage constructEmailMessage(final CustomApplicationEvent event, final UserEnt user) {
	        final String recipientAddress = user.getEmail();
	        final String subject = "Registration Completed";
	        final String message = "You registered successfully. We will send you a confirmation message to your email account.";
	        final SimpleMailMessage email = new SimpleMailMessage();
	        email.setTo(recipientAddress);
	        email.setSubject(subject);
	        email.setText(message + "Your username and password   " + user.getUsername()+":"+user.getPassword());
	        email.setFrom(env.getProperty("support.email"));
	        return email;
	    }
	}
