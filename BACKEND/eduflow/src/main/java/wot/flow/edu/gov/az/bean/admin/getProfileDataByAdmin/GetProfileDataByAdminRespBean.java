/**
 * 
 */
package wot.flow.edu.gov.az.bean.admin.getProfileDataByAdmin;

import wot.flow.edu.gov.az.bean.admin.PersonDtoByAdmin;
import wot.flow.edu.gov.az.utilities.bean.Response;
import wot.flow.edu.gov.az.utilities.enums.ErrorHandl;

/**
 * @author Alqayev Taleh
 *
 * 2017 Nov 19, 2017 11:00:47 PM
 */
public class GetProfileDataByAdminRespBean extends Response {
	
	private PersonDtoByAdmin person;
	
	public PersonDtoByAdmin getPerson() {
		return person;
	}

	public void setPerson(PersonDtoByAdmin person) {
		this.person = person;
	}

	public GetProfileDataByAdminRespBean(ErrorHandl coreMessage){
		super(coreMessage.getErrorCode() , coreMessage.toString());
	}
	
}
