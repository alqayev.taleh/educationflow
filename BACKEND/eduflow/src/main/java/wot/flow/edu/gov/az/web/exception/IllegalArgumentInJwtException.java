/**
 * 
 */
package wot.flow.edu.gov.az.web.exception;

/**
 * @author Taleh-PC
 * 2017 Nov 9, 2017 3:23:04 PM
 */
public class IllegalArgumentInJwtException extends RuntimeException{
	private static final long serialVersionUID = 1363312971559580524L;
	
	public IllegalArgumentInJwtException() {
		super();
	}
}
