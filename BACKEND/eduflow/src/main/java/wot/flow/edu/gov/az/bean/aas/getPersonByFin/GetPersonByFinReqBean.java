/**
 * 
 */
package wot.flow.edu.gov.az.bean.aas.getPersonByFin;

import wot.flow.edu.gov.az.utilities.bean.Request;

/**
 * @author Alqayev Taleh
 *
 * 2017 Nov 15, 2017 9:23:14 PM
 */
public class GetPersonByFinReqBean extends Request {
    
	private String fin;
	
	private String idCardsNo;
	
	public String getIdCardsNo() {
		return idCardsNo;
	}

	public void setIdCardsNo(String idCardsNo) {
		this.idCardsNo = idCardsNo;
	}

	public String getFin() {
		return fin;
	}

	public void setFin(String fin) {
		this.fin = fin;
	}
}
