package wot.flow.edu.gov.az.persistence.entities.aas;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

/**
*
* @author TALEH ALGAYEV
*/

@Entity
@Table(schema = "aas" , name = "aas_service")
public class ServiceEnt implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 11312L;

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@Column(name = "SERVICE_NAME")
	private String serviceName;
	
	@ManyToMany(mappedBy = "services")
	private Set<RoleEnt> roles;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getServiceName() {
		return serviceName;
	}

	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}

	public Set<RoleEnt> getRoles() {
		return roles;
	}

	public void setRoles(Set<RoleEnt> roles) {
		this.roles = roles;
	}

	@Override
	public String toString() {
		return "ServiceEnt [id=" + id + ", serviceName=" + serviceName + ", roles=" + roles + "]";
	}
}
