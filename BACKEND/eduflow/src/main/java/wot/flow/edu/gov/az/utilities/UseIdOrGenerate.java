/**
 * 
 */
package wot.flow.edu.gov.az.utilities;

import java.io.Serializable;

import org.hibernate.HibernateException;
import org.hibernate.engine.spi.SessionImplementor;
import org.hibernate.id.IdentityGenerator;

import wot.flow.edu.gov.az.persistence.entities.BaseEnt;
import wot.flow.edu.gov.az.persistence.entities.aas.PersonEnt;

/**
 * @author Alqayev Taleh
 *
 * 2017 Nov 11, 2017 2:29:49 AM
 */
//public class UseIdOrGenerate extends IdentityGenerator {
//
//	@Override
//	public Serializable generate(SessionImplementor session, Object obj) throws HibernateException {
//		if (obj == null) throw new HibernateException(new NullPointerException()) ;
//
//		if ((( (PersonEnt) obj).getOid()) == 0) {
//			Serializable id = super.generate(session, obj) ;
//			return id;
//		} else {
//			return (( (PersonEnt) obj).getOid());
//
//		}
//	}
//}

