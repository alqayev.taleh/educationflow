package wot.flow.edu.gov.az.persistence.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;

import wot.flow.edu.gov.az.config.security.jwt.JwtUser;
import wot.flow.edu.gov.az.persistence.dao.UserRepository;
import wot.flow.edu.gov.az.web.exception.UserEnabledException;
import wot.flow.edu.gov.az.web.exception.UserLockedException;
import wot.flow.edu.gov.az.web.exception.UserNotFoundException;

/**
 * Created by nydiarra on 06/05/17.
 */
@Service("customUserDetailsService")
@Transactional
public class CustomUserDetailsService implements UserDetailsService {
    
	@Autowired
    private UserRepository userRepository;

	@Override
	public JwtUser loadUserByUsername(final String email) { 
		
		final Optional<List<JwtUser>> userWithRoles = Optional.ofNullable( userRepository.findJwtUserByUsername(email));
		final Optional<JwtUser> user = userWithRoles.get().stream()
				                                          .map(u -> new JwtUser(
															  u.isAccountNonLocked()
															, u.isEnabled()
															, u.getPassword()
															, u.getUsername()
															, userWithRoles.get()
															               .stream()
															               .map(ur -> ur.getDbAuthorities())
															               .collect(Collectors.toList()))).findFirst();
		user.orElseThrow(UserNotFoundException::new);
		if (!user.filter(x ->x.isAccountNonLocked()).isPresent()) throw new UserLockedException();
		if (!user.filter(x ->x.isEnabled()).isPresent()) throw new UserEnabledException();
		return user.get();
	}
 
}
