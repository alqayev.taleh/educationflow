package wot.flow.edu.gov.az.utilities.enums;

import java.util.Arrays;
import java.util.List;

public enum ErrorHandl {
	SUCCESS(0),
	USER_ALREADY_EXIST(1),
	USER_NOT_FOUND(2),
	INVALID_HEADER(3),
	ILLEGAL_ARGUMENT_IN_JWT(4),
	EXPIRED_JWT(5),
	ACCOUNT_LOCKED(6),
	ACCOUNT_ENABLED(7),
	INVALID_JSON_FORMAT(8),
	WRON_DATE(9),
	PERSON_NOT_FOUND(10),
	FIN_OR_IDCARDSNO_ALREADY_EXIST(11),
	FILL_ID_CARDS_NO_OR_FIN(12),
	DUPLICATE_USERNAME_OR_PERSON_TYPE(13),
	EMAIL_OR_USERNAME_ALREADY_EXIST(14),
	NOTIFICATION_NOT_FOUND(15),
	ROLE_NOT_FOUND(16),
	PERSON_TYPE_ALREADY_EXIST(17),
	BADCREDENTIALS(18),
	DICTIONARY_DETAIL_NOT_FOUND(19),
	GENERAL_ERROR(666);
	
	private int errorCode;

	private ErrorHandl(int errorCode) {
		this.setErrorCode(errorCode);
	}

	private void setErrorCode(int errorCode) {
		this.errorCode = errorCode;
	}

	public int getErrorCode() {
		return errorCode;
	}


	public static boolean isCoreMessages(String value) {
		for (ErrorHandl me : ErrorHandl.values()) {
			if (String.valueOf(me.getErrorCode()).equalsIgnoreCase(value))
				return true;
		}
		return false;
	}

	public static ErrorHandl getObject(String value) {
		for (ErrorHandl me : ErrorHandl.values()) {
			if (String.valueOf(me.getErrorCode()).equalsIgnoreCase(value))
				return me;
		}
		return GENERAL_ERROR;
	}

	public static int getValue(String name) {
		for (ErrorHandl me : ErrorHandl.values()) {
			if (me.name().equalsIgnoreCase(name))
				return me.getErrorCode();
		}
		return GENERAL_ERROR.errorCode;
	}

	public List<ErrorHandl> getValues(){
		return Arrays.asList(ErrorHandl.values());
	}
}
