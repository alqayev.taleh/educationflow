package wot.flow.edu.gov.az.persistence.entities.aas;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(schema = "aas" , name ="aas_roles")
public class RoleEnt {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	@Column(name = "order_by")
	private Long orderBy;
	
	@Column(name = "r_name")
	private String name;
	
	@JsonIgnore
	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(name = "AAS_ROLE_TO_SERVICE", joinColumns = { @JoinColumn(name = "ROLE_ID") }, inverseJoinColumns = {
			@JoinColumn(name = "SERVICE_ID") })
	private Set<ServiceEnt> services;
	
	@JsonIgnore
	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(name = "AAS_MENU_TO_ROLE", joinColumns = { @JoinColumn(name = "ROLE_ID") }, inverseJoinColumns = {
			@JoinColumn(name = "MENU_ID") })
	private Set<MenuEnt> menus;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id =id;
	}

	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getOrderBy() {
		return orderBy;
	}

	public void setOrderBy(Long orderBy) {
		this.orderBy = orderBy;
	}
	
	

	public Set<ServiceEnt> getServices() {
		return services;
	}

	public void setServices(Set<ServiceEnt> services) {
		this.services = services;
	}

	public Set<MenuEnt> getMenus() {
		return menus;
	}

	public void setMenus(Set<MenuEnt> menus) {
		this.menus = menus;
	}

	@Override
	public String toString() {
		return "RoleEnt [oid=" + id + ", orderBy=" + orderBy + ", name=" + name + ", services=" + services + ", menus="
				+ menus + "]";
	}
}
