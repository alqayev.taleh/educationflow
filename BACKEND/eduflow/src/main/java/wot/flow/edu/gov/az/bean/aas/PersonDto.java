/**
 * 
 */
package wot.flow.edu.gov.az.bean.aas;

/**
 * @author Alqayev Taleh
 *
 * 2017 Nov 14, 2017 12:03:41 AM
 */
public class PersonDto {
	
	private Long personId;

	private String birthday;

	private String citizenship;

	private String fin;

	private String gender;

	private String idCardsNo;

	private String middlename;

	private String name;

	private String surname;
	
	

	public PersonDto(Long personId, String birthday, String citizenship, String fin, String gender, String idCardsNo,
			String middlename, String name, String surname) {
		super();
		this.personId = personId;
		this.birthday = birthday;
		this.citizenship = citizenship;
		this.fin = fin;
		this.gender = gender;
		this.idCardsNo = idCardsNo;
		this.middlename = middlename;
		this.name = name;
		this.surname = surname;
	}

	public Long getPersonId() {
		return personId;
	}

	public void setPersonId(Long personId) {
		this.personId = personId;
	}

	public String getBirthday() {
		return birthday;
	}

	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}

	public String getCitizenship() {
		return citizenship;
	}

	public void setCitizenship(String citizenship) {
		this.citizenship = citizenship;
	}

	public String getFin() {
		return fin;
	}

	public void setFin(String fin) {
		this.fin = fin;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getIdCardsNo() {
		return idCardsNo;
	}

	public void setIdCardsNo(String idCardsNo) {
		this.idCardsNo = idCardsNo;
	}

	public String getMiddlename() {
		return middlename;
	}

	public void setMiddlename(String middlename) {
		this.middlename = middlename;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}


}
