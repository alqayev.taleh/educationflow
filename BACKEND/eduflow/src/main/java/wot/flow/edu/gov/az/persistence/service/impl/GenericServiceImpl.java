package wot.flow.edu.gov.az.persistence.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import wot.flow.edu.gov.az.persistence.dao.NotificationsRepository;
import wot.flow.edu.gov.az.persistence.dao.UserRepository;
import wot.flow.edu.gov.az.persistence.entities.aas.NotificationsEnt;
import wot.flow.edu.gov.az.persistence.entities.aas.UserEnt;
import wot.flow.edu.gov.az.persistence.service.GenericService;

/**
 * Created by nydiarra on 07/05/17.
 */
@Service
public class GenericServiceImpl implements GenericService {
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private NotificationsRepository randomCityRepository;

    @Override
    public UserEnt findByEmail(String email) {
        return userRepository.findByUsername(email);
    }

    @Override
    public List<UserEnt> findAllUsers() {
        return (List<UserEnt>)userRepository.findAll();
    }

    @Override
    public List<NotificationsEnt> findAllNotificationsEnt() {
        return (List<NotificationsEnt>)randomCityRepository.findAll();
    }
}
