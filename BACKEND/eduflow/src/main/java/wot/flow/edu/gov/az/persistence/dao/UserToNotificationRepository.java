///**
// * 
// */
//package wot.flow.edu.gov.az.persistence.dao;
//
//import org.springframework.data.jpa.repository.JpaRepository;
//
//import wot.flow.edu.gov.az.persistence.entities.aas.UserToNotificationEnt;
//
///**
// * @author Alqayev Taleh
// *
// * 2017 Nov 12, 2017 12:33:06 AM
// */
//public interface UserToNotificationRepository extends JpaRepository<UserToNotificationEnt, Long>{
//
//}
