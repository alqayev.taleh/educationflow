/**
 * 
 */
package wot.flow.edu.gov.az.persistence.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import wot.flow.edu.gov.az.persistence.entities.aas.ParentEnt;

/**
 * @author Alqayev Taleh
 *
 * 2017 Nov 18, 2017 10:05:42 PM
 */
public interface ParentRepository extends JpaRepository<ParentEnt, Long> {

}
