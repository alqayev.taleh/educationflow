package wot.flow.edu.gov.az.persistence.entities;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
public class BaseEnt {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
    private long id ;
	
	@Column(name = "create_date")
    private String createDate;
	
	@Column(name = "update_date")
    private String updateDate;
	
	@Column(name = "state")
    private Integer state;
	
	@Column(name = "auth_id")
	private Long authId;

	public long getId() {
		return id;
	}

	public void setOid(long id) {
		this.id = id;
	}

	public String getCreateDate() {
		return createDate;
	}

	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}

	public String getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(String updateDate) {
		this.updateDate = updateDate;
	}

	public Integer getState() {
		return state;
	}

	public void setState(Integer state) {
		this.state = state;
	}

	public Long getAuthId() {
		return authId;
	}

	public void setAuthId(Long authId) {
		this.authId = authId;
	}

	@Override
	public String toString() {
		return "BaseEnt [oid=" + id + ", createDate=" + createDate + ", updateDate=" + updateDate + ", state=" + state
				+ ", authId=" + authId + "]";
	}
}
