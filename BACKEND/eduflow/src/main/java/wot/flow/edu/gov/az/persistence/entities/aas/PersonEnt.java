package wot.flow.edu.gov.az.persistence.entities.aas;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import wot.flow.edu.gov.az.persistence.entities.BaseEnt;

@Entity
@Table(schema = "aas" , name = "aas_person")
public class PersonEnt extends BaseEnt implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 11312L;
   
	@Column(name = "p_name")
	private String name;
	
	@Column(name = "p_surname")
	private String surname;
	
	@Column(name = "p_middlename")
	private String middlename;
	
	@Column(name = "p_citizenship")
	private String citizenship;
	
	@Column(name = "p_fin")
	private String fin;
	
	@Column(name = "p_id_cards_no")
	private String idCardsNo;
	
	@Column(name = "p_gender")
	private String gender;
	
	@Column(name = "p_birthday")
	private String birthday;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getMiddlename() {
		return middlename;
	}

	public void setMiddlename(String middlename) {
		this.middlename = middlename;
	}

	public String getCitizenship() {
		return citizenship;
	}

	public void setCitizenship(String citizenship) {
		this.citizenship = citizenship;
	}

	public String getFin() {
		return fin;
	}

	public void setFin(String fin) {
		this.fin = fin;
	}

	public String getIdCardsNo() {
		return idCardsNo;
	}

	public void setIdCardsNo(String idCardsNo) {
		this.idCardsNo = idCardsNo;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getBirthday() {
		return birthday;
	}

	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}

	@Override
	public String toString() {
		return "PersonEnt [name=" + name + ", surname=" + surname + ", middlename=" + middlename + ", citizenship="
				+ citizenship + ", fin=" + fin + ", idCardsNo=" + idCardsNo
				+", gender=" + gender + ", birthday=" + birthday + "]";
	}
}
