/**
 * 
 */
package wot.flow.edu.gov.az.bean.aas.registerOrUpdatePerson;

/**
 * @author Alqayev Taleh
 *
 *eger tekce personda deyishiklik olarsa userbean bosh gonderilir 
 *
 * 2017 Nov 10, 2017 10:12:47 PM
 */
public class PersonBean {
	
	private Long personOid;
	
	private String citizenship;
	   
    private String name;
    
    private String surname;
    
    private String birthday;
    
    private String gender;
    
    private String middlename;
    
    private String fin;
    
    private String idCardsNo;

	public Long getPersonOid() {
		return personOid;
	}

	public void setPersonOid(Long personOid) {
		this.personOid = personOid;
	}

	public String getCitizenship() {
		return citizenship;
	}

	public void setCitizenship(String citizenship) {
		this.citizenship = citizenship;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getBirthday() {
		return birthday;
	}

	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getMiddlename() {
		return middlename;
	}

	public void setMiddlename(String middlename) {
		this.middlename = middlename;
	}

	public String getFin() {
		return fin;
	}

	public void setFin(String fin) {
		this.fin = fin;
	}

	public String getIdCardsNo() {
		return idCardsNo;
	}

	public void setIdCardsNo(String idCardsNo) {
		this.idCardsNo = idCardsNo;
	}
}
