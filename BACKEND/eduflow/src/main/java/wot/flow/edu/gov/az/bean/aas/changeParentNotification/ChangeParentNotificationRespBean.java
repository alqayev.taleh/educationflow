/**
 * 
 */
package wot.flow.edu.gov.az.bean.aas.changeParentNotification;

import wot.flow.edu.gov.az.utilities.bean.Response;
import wot.flow.edu.gov.az.utilities.enums.ErrorHandl;

/**
 * @author Alqayev Taleh
 *
 * 2017 Nov 18, 2017 10:46:55 PM
 */
public class ChangeParentNotificationRespBean extends Response {
	
	public ChangeParentNotificationRespBean(ErrorHandl coreMessage){
		super(coreMessage.getErrorCode() , coreMessage.toString());
	}
	
}
