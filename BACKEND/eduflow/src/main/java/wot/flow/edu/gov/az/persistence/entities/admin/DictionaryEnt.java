/**
 * 
 */
package wot.flow.edu.gov.az.persistence.entities.admin;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author Alqayev Taleh
 *
 * 2017 Nov 12, 2017 11:13:15 PM
 */
@Entity
@Table(schema = "admin" , name = "adm_dictionary")
public class DictionaryEnt {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;
	
	@Column(name = "name")
	private String name;
	
	@Column(name = "order_by")
	private Integer orderBy;
	
	@Column(name = "description")
	private String description;
	
	@Column(name = "state")
	private Integer state;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getOrderBy() {
		return orderBy;
	}

	public void setOrderBy(Integer orderBy) {
		this.orderBy = orderBy;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getState() {
		return state;
	}

	public void setState(Integer state) {
		this.state = state;
	}

	@Override
	public String toString() {
		return "Dictionary [id=" + id + ", name=" + name + ", orderBy=" + orderBy + ", description=" + description
				+ ", state=" + state + "]";
	}
}
