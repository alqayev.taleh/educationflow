/**
 * 
 */
package wot.flow.edu.gov.az.persistence.entities.admin;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * @author Alqayev Taleh
 *
 * 2017 Nov 12, 2017 11:13:29 PM
 */
@Entity
@Table(schema = "admin" , name = "adm_dictionary_detail")
public class DictionaryDetailEnt {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;
	
	@ManyToOne
	@JoinColumn(name = "dic_id")
	private DictionaryEnt dictionary;
	
	@Column(name = "name")
	private String name;
	
	@Column(name = "order_by")
	private Integer orderBy;
	
	@Column(name = "description")
	private String description;
	
	@Column(name = "state")
	private Integer state;
	
	public DictionaryEnt getDictionary() {
		return dictionary;
	}

	public void setDictionary(DictionaryEnt dictionary) {
		this.dictionary = dictionary;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getOrderBy() {
		return orderBy;
	}

	public void setOrderBy(Integer orderBy) {
		this.orderBy = orderBy;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getState() {
		return state;
	}

	public void setState(Integer state) {
		this.state = state;
	}

	@Override
	public String toString() {
		return "DictionaryDetail [id=" + id + ", dictionary=" + dictionary + ", name=" + name + ", orderBy=" + orderBy
				+ ", description=" + description + ", state=" + state + "]";
	}
}
