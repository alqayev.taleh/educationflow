package wot.flow.edu.gov.az.bean.aas.auth;

import wot.flow.edu.gov.az.utilities.bean.Response;


/**
 * @author Taleh-PC
 * 2017 Nov 3, 2017 11:00:47 PM
 */
public class AuthRespBean extends Response {
	
	private String token;

	public AuthRespBean(final String token) {
		this.token = token;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}
}
