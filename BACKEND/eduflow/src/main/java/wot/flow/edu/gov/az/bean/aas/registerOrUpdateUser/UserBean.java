package wot.flow.edu.gov.az.bean.aas.registerOrUpdateUser;

import java.util.List;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * @author Alqayev Taleh
 *
 * 2017 Nov 10, 2017 10:12:28 PM
 */
public class UserBean {

	private String email;
	
	private String username;
	
	private String password;
	
	private String passwordRetype;
	
	private Long personType;
	
	private Set<Long> notifications;
	
	private Set<Long> roles;

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPasswordRetype() {
		return passwordRetype;
	}

	public void setPasswordRetype(String passwordRetype) {
		this.passwordRetype = passwordRetype;
	}

	public Long getPersonType() {
		return personType;
	}

	public void setPersonType(Long personType) {
		this.personType = personType;
	}

	public Set<Long> getNotifications() {
		return notifications;
	}

	public void setNotifications(Set<Long> notifications) {
		this.notifications = notifications;
	}

	public Set<Long> getRoles() {
		return roles;
	}

	public void setRoles(Set<Long> roles) {
		this.roles = roles;
	}
}
