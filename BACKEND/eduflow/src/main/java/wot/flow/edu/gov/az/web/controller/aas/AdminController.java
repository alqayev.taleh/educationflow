/**
 * 
 */
package wot.flow.edu.gov.az.web.controller.aas;

import java.io.IOException;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import wot.flow.edu.gov.az.bean.aas.auth.AuthRespBean;
import wot.flow.edu.gov.az.bean.admin.PersonDtoByAdmin;
import wot.flow.edu.gov.az.bean.admin.UserDtoByAdmin;
import wot.flow.edu.gov.az.bean.admin.WorkerDto;
import wot.flow.edu.gov.az.bean.admin.getProfileDataByAdmin.GetProfileDataByAdminReqBean;
import wot.flow.edu.gov.az.bean.admin.getProfileDataByAdmin.GetProfileDataByAdminRespBean;
import wot.flow.edu.gov.az.config.security.jwt.JwtTokenUtil;
import wot.flow.edu.gov.az.config.security.override.CustomAuthenticationProvider;
import wot.flow.edu.gov.az.persistence.dao.PersonRepository;
import wot.flow.edu.gov.az.persistence.entities.aas.RoleEnt;
import wot.flow.edu.gov.az.persistence.entities.aas.UserEnt;
import wot.flow.edu.gov.az.persistence.service.ParentService;
import wot.flow.edu.gov.az.persistence.service.PersonService;
import wot.flow.edu.gov.az.persistence.service.UserService;
import wot.flow.edu.gov.az.utilities.Servant;
import wot.flow.edu.gov.az.utilities.enums.DATEFORMAT;
import wot.flow.edu.gov.az.web.exception.UserNotFoundException;

/**
 * @author Alqayev Taleh
 *
 * 2017 Nov 19, 2017 11:29:54 PM
 */

@RestController()
@RequestMapping(path = "/admin")
public class AdminController {
		
		@Autowired
		GetProfileDataByAdminRespBean getProfileDataByAdminRespBean;

		@Autowired
		private UserDetailsService userDetailsService;
		
		@Autowired
		private PersonService personService;
		
		@Autowired
		private UserService userService;
		
		@Autowired
		private ParentService parentService;
		
		@Autowired
		private PersonRepository personRepository;
		
		@Autowired
	    ApplicationEventPublisher eventPublisher;

		@Autowired
		private CustomAuthenticationProvider authManager;

		@Autowired
		private JwtTokenUtil jwtTokenUtil;

		@Value("${jwt.header}")
		private String tokenHeader;

		@PostMapping(value = "/getProfileDataByAdmin")
//		 @PreAuthorize("hasRole('ADMIN')")
		public ResponseEntity<GetProfileDataByAdminRespBean> createAuthenticationToken(GetProfileDataByAdminReqBean requestBean,
				HttpServletRequest request) throws AuthenticationException, IOException {

			String username = jwtTokenUtil.getUsernameFromToken(jwtTokenUtil.getTokenFromHeader(request));
			
			Optional<UserEnt> userOpt = Optional.ofNullable(userService.findUserByUserName(username));
			userOpt.orElseThrow(UserNotFoundException::new);
			PersonDtoByAdmin person = userOpt.map(user -> new PersonDtoByAdmin(user.getPerson().getId()
					, Servant.format(user.getPerson().getBirthday(), DATEFORMAT.DATABASE_DATE.getDate(), DATEFORMAT.FRONT_DATE.getDate()) 
					, user.getPerson().getCitizenship()
					, user.getPerson().getFin()
					, user.getPerson().getGender()
					, user.getPerson().getIdCardsNo()
					, user.getPerson().getMiddlename()
					, user.getPerson().getName()
					, user.getPerson().getSurname()
					, new UserDtoByAdmin(user.getId()
							, user.getEmail()
							, user.getUsername()
							, Objects.isNull(user.getPersonType())?0:user.getPersonType().getId()
							, user.isAccountNonLocked()
							, user.isEnabled()
							, user.getRoles().stream()
							                 .map(RoleEnt::getId)
							                 .collect(Collectors.toSet())
							, user.getUserNotifications().stream()
							                             .map(notf -> notf.getId())
							                             .collect(Collectors.toSet())
							, new WorkerDto(1l)))).get();
			getProfileDataByAdminRespBean.setPerson(person);
			return ResponseEntity.ok(getProfileDataByAdminRespBean);
		}
		
}
