/**
 * 
 */
package wot.flow.edu.gov.az.bean.admin;

import java.util.Set;

/**
 * @author Alqayev Taleh
 *
 * 2017 Nov 19, 2017 11:04:34 PM
 */
public class UserDtoByAdmin {

	private Long id;
	
	private String email;
	
	private String username;
	
	private Long personType;
	
	private Boolean accountNonLocked;
	
	private Boolean enabled;
	
	private Set<Long> roles;
	
	private Set<Long> notifications;
	
	private WorkerDto worker;

	public UserDtoByAdmin(Long id, String email, String username, Long personType, Boolean accountNonLocked, Boolean enabled,
			Set<Long> roles, Set<Long> notifications,WorkerDto worker) {
		super();
		this.id = id;
		this.email = email;
		this.username = username;
		this.personType = personType;
		this.accountNonLocked = accountNonLocked;
		this.enabled = enabled;
		this.roles = roles;
		this.notifications = notifications;
		this.worker = worker;
	}
	
	public WorkerDto getWorker() {
		return worker;
	}



	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public Long getPersonType() {
		return personType;
	}

	public void setPersonType(Long personType) {
		this.personType = personType;
	}

	public Boolean getAccountNonLocked() {
		return accountNonLocked;
	}

	public void setAccountNonLocked(Boolean accountNonLocked) {
		this.accountNonLocked = accountNonLocked;
	}

	public Boolean getEnabled() {
		return enabled;
	}

	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}

	public Set<Long> getRoles() {
		return roles;
	}

	public void setRoles(Set<Long> roles) {
		this.roles = roles;
	}

	public Set<Long> getNotifications() {
		return notifications;
	}

	public void setNotifications(Set<Long> notifications) {
		this.notifications = notifications;
	}
}
