/**
 * 
 */
package wot.flow.edu.gov.az.bean.aas.addParent;

import wot.flow.edu.gov.az.utilities.bean.Request;

/**
 * @author Alqayev Taleh
 *
 * 2017 Nov 18, 2017 9:42:36 PM
 */
public class AddParentReqBean extends Request {
	
	private ParentBean parent;

	public ParentBean getParent() {
		return parent;
	}

	public void setParent(ParentBean parent) {
		this.parent = parent;
	}
}