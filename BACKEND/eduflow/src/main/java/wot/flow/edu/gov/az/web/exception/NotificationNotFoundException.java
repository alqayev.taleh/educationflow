/**
 * 
 */
package wot.flow.edu.gov.az.web.exception;

/**
 * @author Alqayev Taleh
 *
 * 2017 Nov 12, 2017 12:37:43 AM
 */
public class NotificationNotFoundException extends RuntimeException{
	private static final long serialVersionUID = 1363312971559580524L;

	public NotificationNotFoundException() {
		super();
	}

	public NotificationNotFoundException(final String message, final Throwable cause) {
		super(message, cause);
	}

	public NotificationNotFoundException(final String message) {
		super(message);
	}

	public NotificationNotFoundException(final Throwable cause) {
		super(cause);
	}	
}
