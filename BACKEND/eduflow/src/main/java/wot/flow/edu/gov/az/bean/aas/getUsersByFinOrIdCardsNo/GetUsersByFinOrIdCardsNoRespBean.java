/**
 * 
 */
package wot.flow.edu.gov.az.bean.aas.getUsersByFinOrIdCardsNo;

import java.util.Set;

import wot.flow.edu.gov.az.bean.aas.UserDto;
import wot.flow.edu.gov.az.utilities.bean.Response;
import wot.flow.edu.gov.az.utilities.enums.ErrorHandl;

/**
 * @author Alqayev Taleh
 *
 * 2017 Nov 18, 2017 8:15:11 PM
 */
public class GetUsersByFinOrIdCardsNoRespBean extends Response {
	
	private Set<UserDto> users ;
	
	public Set<UserDto> getUsers() {
		return users;
	}

	public void setUsers(Set<UserDto> users) {
		this.users = users;
	}

	public GetUsersByFinOrIdCardsNoRespBean(ErrorHandl coreMessage){
		super (coreMessage.getErrorCode() , coreMessage.toString());
	}
}
