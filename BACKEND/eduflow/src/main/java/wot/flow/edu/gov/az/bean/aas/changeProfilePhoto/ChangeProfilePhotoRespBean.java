/**
 * 
 */
package wot.flow.edu.gov.az.bean.aas.changeProfilePhoto;

import wot.flow.edu.gov.az.utilities.bean.Response;
import wot.flow.edu.gov.az.utilities.enums.ErrorHandl;

/**
 * @author Alqayev Taleh
 *
 * 2017 Nov 25, 2017 5:16:21 PM
 */
public class ChangeProfilePhotoRespBean extends Response {
	public ChangeProfilePhotoRespBean(ErrorHandl coreMessage){
		super(coreMessage.getErrorCode() , coreMessage.toString());
	}
}
