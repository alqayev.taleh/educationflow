package wot.flow.edu.gov.az.persistence.entities.aas;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(schema = "aas" , name = "AAS_MENU")
public class MenuEnt implements Serializable {

	private static final long serialVersionUID = 113123L;

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column(name = "MENU_NAME")
	private String name;

	@ManyToMany(mappedBy = "menus")
	private Set<RoleEnt> roles;

	public MenuEnt() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}


	public Set<RoleEnt> getRoles() {
		return roles;
	}

	public void setRoles(Set<RoleEnt> roles) {
		this.roles = roles;
	}

	@Override
	public String toString() {
		return "Menu [id=" + id + ", menuName=" + name + ", roles=" + roles + "]";
	}

}

