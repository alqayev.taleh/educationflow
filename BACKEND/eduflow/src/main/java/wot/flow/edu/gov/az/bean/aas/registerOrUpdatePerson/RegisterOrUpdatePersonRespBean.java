package wot.flow.edu.gov.az.bean.aas.registerOrUpdatePerson;

import wot.flow.edu.gov.az.utilities.bean.Response;
import wot.flow.edu.gov.az.utilities.enums.ErrorHandl;

public class RegisterOrUpdatePersonRespBean extends Response{
	
	public RegisterOrUpdatePersonRespBean(ErrorHandl coreMessage){
		super( coreMessage.getErrorCode(),coreMessage.toString());
	}
}
