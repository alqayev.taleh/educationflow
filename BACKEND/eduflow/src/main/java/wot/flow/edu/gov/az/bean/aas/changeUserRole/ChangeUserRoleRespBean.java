/**
 * 
 */
package wot.flow.edu.gov.az.bean.aas.changeUserRole;

import wot.flow.edu.gov.az.utilities.bean.Response;
import wot.flow.edu.gov.az.utilities.enums.ErrorHandl;

/**
 * @author Alqayev Taleh
 *
 * 2017 Nov 16, 2017 11:52:24 PM
 */
public class ChangeUserRoleRespBean extends Response {
	
	public ChangeUserRoleRespBean(ErrorHandl coreMessage){
		super ( coreMessage.getErrorCode(),coreMessage.toString());
	}
	
}
