package wot.flow.edu.gov.az.bean.aas.registerOrUpdatePerson;

import wot.flow.edu.gov.az.utilities.bean.Request;

/**
 * @author Alqayev Taleh
 *
 * 2017 Nov 10, 2017 10:05:57 PM
 */
public class RegisterOrUpdatePersonReqBean extends Request{
   
	private PersonBean person;

	public PersonBean getPerson() {
		return person;
	}

	public void setPerson(PersonBean person) {
		this.person = person;
	}
}
