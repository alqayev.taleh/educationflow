/**
 * 
 */
package wot.flow.edu.gov.az.bean.aas.getInfoAfterLogin;

/**
 * @author Alqayev Taleh
 *
 * 2017 Dec 9, 2017 11:53:08 PM
 */
public class AfterLoginBean {
	private String middleName;

	private String userName;

	private String photo;

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPhoto() {
		return photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}
}
