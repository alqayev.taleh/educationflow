package wot.flow.edu.gov.az.config.jpa;

import java.util.Properties;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.zaxxer.hikari.HikariDataSource;


@Configuration
@EnableJpaRepositories(basePackages = "wot.flow.edu.gov.az.persistence.dao")
@ComponentScan({ "wot.flow.edu.gov.az.persistence" })
@ConfigurationProperties("classpath:application.yml")
@EnableTransactionManagement
public class JpaConfiguration {
	@Autowired
	private Environment environment;

	public JpaConfiguration() {
		super();
	}

	/*
	 * Entity Manager Factory setup.
	 */
	@Bean
	public LocalContainerEntityManagerFactoryBean entityManagerFactory() {
		LocalContainerEntityManagerFactoryBean factoryBean = new LocalContainerEntityManagerFactoryBean();
		factoryBean.setDataSource(dataSource());
		factoryBean.setPackagesToScan(new String[] { "wot.flow.edu.gov.az.persistence.entities" });
		factoryBean.setJpaVendorAdapter(jpaVendorAdapter());
		factoryBean.setJpaProperties(jpaProperties());
		return factoryBean;
	}
	/*
	 * Provider specific adapter.
	 */
	@Bean
	public JpaVendorAdapter jpaVendorAdapter() {
		return new HibernateJpaVendorAdapter();
	}
	
	@Bean
	public HikariDataSource dataSource() {
		final HikariDataSource ds = new HikariDataSource();
		ds.setMaximumPoolSize(Integer.valueOf(environment.getProperty("spring.datasource.postgres.maxPoolSize")));
		ds.setDataSourceClassName(environment.getProperty("spring.datasource.postgres.driverClassName"));
		ds.addDataSourceProperty("url", environment.getProperty("spring.datasource.postgres.url"));
		ds.addDataSourceProperty("user", environment.getProperty("spring.datasource.postgres.username"));
		ds.addDataSourceProperty("password", environment.getProperty("spring.datasource.postgres.password"));
		return ds;
	}
	
	/*
	 * Here you can specify any provider specific properties.
	 */
	private Properties jpaProperties() {
		Properties properties = new Properties();
		properties.put("hibernate.dialect", environment.getRequiredProperty("spring.datasource.postgres.hibernate.dialect"));
		properties.put("hibernate.hbm2ddl.auto", environment.getRequiredProperty("spring.datasource.postgres.hibernate.hbm2ddl.method"));
		properties.put("hibernate.show_sql", environment.getRequiredProperty("spring.datasource.postgres.hibernate.show_sql"));
		properties.put("hibernate.format_sql", environment.getRequiredProperty("spring.datasource.postgres.hibernate.format_sql"));
		if(StringUtils.isNotEmpty(environment.getRequiredProperty("spring.datasource.postgres.defaultSchema"))){
			properties.put("hibernate.default_schema", environment.getRequiredProperty("spring.datasource.postgres.defaultSchema"));
		}
		return properties;
	}
}
