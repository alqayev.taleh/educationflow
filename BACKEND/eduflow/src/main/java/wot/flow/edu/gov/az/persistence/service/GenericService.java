package wot.flow.edu.gov.az.persistence.service;

import java.util.List;

import wot.flow.edu.gov.az.persistence.entities.aas.NotificationsEnt;
import wot.flow.edu.gov.az.persistence.entities.aas.UserEnt;

/**
 * Created by nydiarra on 06/05/17.
 */
public interface GenericService {
	UserEnt findByEmail(String email);

    List<UserEnt> findAllUsers();

    List<NotificationsEnt> findAllNotificationsEnt();
}
