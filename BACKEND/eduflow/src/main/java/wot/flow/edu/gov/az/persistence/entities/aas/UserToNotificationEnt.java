//package wot.flow.edu.gov.az.persistence.entities.aas;
//
//import javax.persistence.Entity;
//import javax.persistence.JoinColumn;
//import javax.persistence.ManyToOne;
//import javax.persistence.Table;
//
//import wot.flow.edu.gov.az.persistence.entities.BaseEnt;
//import wot.flow.edu.gov.az.persistence.entities.admin.DictionaryDetail;
//
//@Entity
//@Table(schema = "aas" , name = "aas_user_to_notification")
//public class UserToNotificationEnt extends BaseEnt{
//
//	@ManyToOne
//    @JoinColumn(name = "user_id")
//    private UserEnt user;
//	
//	@ManyToOne
//    @JoinColumn(name = "notification_id")
//    private DictionaryDetail notification;
//
//	public UserEnt getUser() {
//		return user;
//	}
//
//	public void setUser(UserEnt user) {
//		this.user = user;
//	}
//
//	public DictionaryDetail getNotification() {
//		return notification;
//	}
//
//	public void setNotification(DictionaryDetail notification) {
//		this.notification = notification;
//	}
//
//	@Override
//	public String toString() {
//		return "UserToNotificationTypeEnt [user=" + user + ", notification=" + notification + "]";
//	}
//
//}
