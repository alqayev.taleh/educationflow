/**
 * 
 */
package wot.flow.edu.gov.az.web.exception;

/**
 * @author Alqayev Taleh
 *
 * 2017 Nov 9, 2017 10:53:45 PM
 */
public class UserLockedException extends RuntimeException {

	private static final long serialVersionUID = 1363312971559580524L;
	
	public UserLockedException() {
        super();
    }

    public UserLockedException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public UserLockedException(final String message) {
        super(message);
    }

    public UserLockedException(final Throwable cause) {
        super(cause);
    }	
}
