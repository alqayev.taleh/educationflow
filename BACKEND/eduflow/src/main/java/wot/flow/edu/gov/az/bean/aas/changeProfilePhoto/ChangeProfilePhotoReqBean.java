/**
 * 
 */
package wot.flow.edu.gov.az.bean.aas.changeProfilePhoto;

import wot.flow.edu.gov.az.utilities.bean.Request;

/**
 * @author Alqayev Taleh
 *
 * 2017 Nov 25, 2017 5:16:01 PM
 */
public class ChangeProfilePhotoReqBean extends Request {
	
	private String photo;

	public String getPhoto() {
		return photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}
}
