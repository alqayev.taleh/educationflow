/**
 * 
 */
package wot.flow.edu.gov.az.bean.aas.getPersons;

import java.util.List;

import wot.flow.edu.gov.az.bean.aas.PersonDto;
import wot.flow.edu.gov.az.utilities.bean.Response;
import wot.flow.edu.gov.az.utilities.enums.ErrorHandl;

/**
 * @author Alqayev Taleh
 *
 * 2017 Nov 13, 2017 11:50:18 PM
 */
public class GetPersonsRespBean extends Response{
	
	private List<PersonDto> persons;
	
	public List<PersonDto> getPersons() {
		return persons;
	}

	public void setPersons(List<PersonDto> persons) {
		this.persons = persons;
	}

	public GetPersonsRespBean(ErrorHandl coreMessage){
		super( coreMessage.getErrorCode() , coreMessage.toString());
	}
}
