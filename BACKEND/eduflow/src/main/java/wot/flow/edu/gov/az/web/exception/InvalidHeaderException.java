/**
 * 
 */
package wot.flow.edu.gov.az.web.exception;

/**
 * @author Taleh-PC
 * 2017 Nov 9, 2017 12:51:25 PM
 */
public class InvalidHeaderException extends RuntimeException {

	private static final long serialVersionUID = 1363312971559580524L;

	public InvalidHeaderException() {
        super();
    }

    public InvalidHeaderException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public InvalidHeaderException(final String message) {
        super(message);
    }

    public InvalidHeaderException(final Throwable cause) {
        super(cause);
    }	
	
}