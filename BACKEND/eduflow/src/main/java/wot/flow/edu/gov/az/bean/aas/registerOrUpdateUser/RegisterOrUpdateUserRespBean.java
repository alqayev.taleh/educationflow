/**
 * 
 */
package wot.flow.edu.gov.az.bean.aas.registerOrUpdateUser;

import wot.flow.edu.gov.az.utilities.bean.Response;
import wot.flow.edu.gov.az.utilities.enums.ErrorHandl;

/**
 * @author Alqayev Taleh
 *
 * 2017 Nov 11, 2017 3:33:14 PM
 */
public class RegisterOrUpdateUserRespBean extends Response{
	public RegisterOrUpdateUserRespBean(ErrorHandl coreMessage){
		super(coreMessage.getErrorCode(), coreMessage.toString());
	}
}
