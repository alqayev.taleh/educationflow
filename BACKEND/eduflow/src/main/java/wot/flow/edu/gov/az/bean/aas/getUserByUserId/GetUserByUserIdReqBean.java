/**
 * 
 */
package wot.flow.edu.gov.az.bean.aas.getUserByUserId;

import wot.flow.edu.gov.az.utilities.bean.Request;

/**
 * @author Alqayev Taleh
 *
 * 2017 Nov 15, 2017 10:28:45 PM
 */
public class GetUserByUserIdReqBean extends Request {
	private Long id;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
}
