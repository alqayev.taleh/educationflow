package wot.flow.edu.gov.az.utilities.bean;

import wot.flow.edu.gov.az.utilities.enums.ErrorHandl;

public class Response {
	
	public static Response SUCCESS = new Response(0, "ok");
	private int code;
	private String message;
	
	public Response(int code,String message){
		this.code = code;
		this.message = message;
	}
	
	public Response(ErrorHandl coreMessage){
		this.code = coreMessage.getErrorCode();
		this.message = coreMessage.toString();
	}
	
	public Response(){

	}
	
	public int getCode() {
		return code;
	}
	public void setCode(int code) {
		this.code = code;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
}
