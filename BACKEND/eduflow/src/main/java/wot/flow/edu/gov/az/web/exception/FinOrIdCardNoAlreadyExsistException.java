/**
 * 
 */
package wot.flow.edu.gov.az.web.exception;

/**
 * @author Alqayev Taleh
 *
 * 2017 Nov 11, 2017 12:35:14 AM
 */
public class FinOrIdCardNoAlreadyExsistException extends RuntimeException{

	private static final long serialVersionUID = 1363312971559580524L;
	
	public FinOrIdCardNoAlreadyExsistException() {
        super();
    }

    public FinOrIdCardNoAlreadyExsistException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public FinOrIdCardNoAlreadyExsistException(final String message) {
        super(message);
    }

    public FinOrIdCardNoAlreadyExsistException(final Throwable cause) {
        super(cause);
    }	
}