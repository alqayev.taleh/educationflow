/**
 * 
 */
package wot.flow.edu.gov.az.web.exception;

/**
 * @author Alqayev Taleh
 *
 * 2017 Nov 12, 2017 1:07:09 AM
 */
public class RoleNotFoundException extends RuntimeException{
	private static final long serialVersionUID = 1363312971559580524L;

	public RoleNotFoundException() {
		super();
	}

	public RoleNotFoundException(final String message, final Throwable cause) {
		super(message, cause);
	}

	public RoleNotFoundException(final String message) {
		super(message);
	}

	public RoleNotFoundException(final Throwable cause) {
		super(cause);
	}	
}
