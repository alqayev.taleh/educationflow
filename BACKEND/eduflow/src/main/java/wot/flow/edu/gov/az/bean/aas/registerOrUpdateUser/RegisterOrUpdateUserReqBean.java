/**
 * 
 */
package wot.flow.edu.gov.az.bean.aas.registerOrUpdateUser;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;

import wot.flow.edu.gov.az.utilities.bean.Request;

/**
 * @author Alqayev Taleh
 *
 * 2017 Nov 11, 2017 3:32:58 PM
 */
public class RegisterOrUpdateUserReqBean extends Request{
	
//	@JsonIgnore
	 private Long personOid;
	 
//	@JsonIgnore
    UserBean user;

	public Long getPersonOid() {
		return personOid;
	}

	public void setPersonOid(Long personOid) {
		this.personOid = personOid;
	}

	public UserBean getUser() {
		return user;
	}

	public void setUser(UserBean user) {
		this.user = user;
	}
}
