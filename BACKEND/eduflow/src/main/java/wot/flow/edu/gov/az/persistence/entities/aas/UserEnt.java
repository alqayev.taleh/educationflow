package wot.flow.edu.gov.az.persistence.entities.aas;

import java.io.Serializable;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import wot.flow.edu.gov.az.persistence.entities.BaseEnt;
import wot.flow.edu.gov.az.persistence.entities.admin.DictionaryDetailEnt;

@Entity
@Table(schema = "aas" , name = "aas_users")
public class UserEnt extends BaseEnt implements UserDetails,Serializable{
	
	private static final long serialVersionUID = 123423L;
	
	@Column(name = "u_last_login_date")
	private String lastLoginDate;
	
	@Column(name = "u_new_login_date")
	private String newLoginDate;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="person_id")
	private PersonEnt person;
	
	@Column(name = "u_email")
	private String email;
	
	@Column(name = "password")
	private String password;
	
	@Column(name = "username")
	private String username;
	
	@Column(name = "photo")
	private String photo;
	
	@ManyToOne
    @JoinColumn(name = "person_type")
    private DictionaryDetailEnt personType;
	
	@Column(name = "LASTPASSWORDRESETDATE")
    private String lastPasswordResetDate;
	
	@Column(name = "account_non_locked")
	private boolean accountNonLocked;
	
	@Column(name = "enabled")
	private boolean enabled;

	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "aas_user_to_role" , joinColumns = {@JoinColumn(name = "user_id" , referencedColumnName = "id")} ,
	inverseJoinColumns = {@JoinColumn(name = "role_id" , referencedColumnName = "id")})
	private Set<RoleEnt> roles = new HashSet<>();
	
	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "aas_user_to_notification" , joinColumns = {@JoinColumn(name = "user_id")} ,
	inverseJoinColumns = {@JoinColumn(name = "notification_id")})
	private Set<DictionaryDetailEnt> userNotifications = new HashSet<>();
	
	public PersonEnt getPerson() {
		return person;
	}
	
	public String getPhoto() {
		return photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}

	public DictionaryDetailEnt getPersonType() {
		return personType;
	}

	public void setPersonType(DictionaryDetailEnt personType) {
		this.personType = personType;
	}

	public String getLastPasswordResetDate() {
		return lastPasswordResetDate;
	}

	public void setLastPasswordResetDate(String lastPasswordResetDate) {
		this.lastPasswordResetDate = lastPasswordResetDate;
	}

	public void setPerson(PersonEnt person) {
		this.person = person;
	}

	public Set<DictionaryDetailEnt> getUserNotifications() {
		return userNotifications;
	}

	public void setUserNotifications(Set<DictionaryDetailEnt> userToNotifications) {
		this.userNotifications = userToNotifications;
	}

	public String getLastLoginDate() {
		return lastLoginDate;
	}

	public void setLastLoginDate(String lastLoginDate) {
		this.lastLoginDate = lastLoginDate;
	}

	public String getNewLoginDate() {
		return newLoginDate;
	}

	public void setNewLoginDate(String newLoginDate) {
		this.newLoginDate = newLoginDate;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}


	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Set<RoleEnt> getRoles() {
		return roles;
	}

	public void setRoles(Set<RoleEnt> roles) {
		this.roles = roles;
	}

	@Override
	public String toString() {
		return "UserEnt [lastLoginDate=" + lastLoginDate + ", newLoginDate=" + newLoginDate + ", person=" + person
				+ ", email=" + email + ", password=" + password + ", username=" + username + ", roles=" + roles
				+ ", userToNotifications=" + userNotifications + "]";
	}

	
	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return accountNonLocked;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}
	
	@Override
	public boolean isEnabled() {
		return enabled;
	}
	
	public void setAccountNonLocked(boolean accountNonLocked) {
		this.accountNonLocked = accountNonLocked;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	private static List<GrantedAuthority> mapToGrantedAuthorities(Collection<RoleEnt> authorities) {
		return authorities.stream().map(authority -> new SimpleGrantedAuthority(authority.getName()))
				.collect(Collectors.toList());
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return mapToGrantedAuthorities(getRoles());
	}
}
