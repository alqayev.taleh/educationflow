/**
 * 
 */
package wot.flow.edu.gov.az.bean.aas.changeUserNotification;

import wot.flow.edu.gov.az.utilities.bean.Response;
import wot.flow.edu.gov.az.utilities.enums.ErrorHandl;

/**
 * @author Alqayev Taleh
 *
 * 2017 Nov 17, 2017 3:47:13 PM
 */
public class ChangeUserNotificationRespBean extends Response{

	public ChangeUserNotificationRespBean(ErrorHandl coreMessage){
		super (coreMessage.getErrorCode() , coreMessage.toString());
	}
	
}
