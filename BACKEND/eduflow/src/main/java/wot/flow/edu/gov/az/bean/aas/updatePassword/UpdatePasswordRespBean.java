/**
 * 
 */
package wot.flow.edu.gov.az.bean.aas.updatePassword;

import wot.flow.edu.gov.az.utilities.bean.Response;
import wot.flow.edu.gov.az.utilities.enums.ErrorHandl;

/**
 * @author Alqayev Taleh
 *
 * 2017 Nov 12, 2017 2:10:29 PM
 */
public class UpdatePasswordRespBean extends Response {
	public UpdatePasswordRespBean(ErrorHandl coreMessage){
		super(coreMessage.getErrorCode(),coreMessage.toString());
	}
}
