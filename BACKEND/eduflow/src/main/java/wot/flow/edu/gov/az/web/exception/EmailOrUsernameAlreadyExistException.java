/**
 * 
 */
package wot.flow.edu.gov.az.web.exception;

/**
 * @author Alqayev Taleh
 *
 * 2017 Nov 11, 2017 5:07:49 PM
 */
public class EmailOrUsernameAlreadyExistException  extends RuntimeException {

	private static final long serialVersionUID = 1363312971559580524L;
	
	public EmailOrUsernameAlreadyExistException() {
        super();
    }

    public EmailOrUsernameAlreadyExistException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public EmailOrUsernameAlreadyExistException(final String message) {
        super(message);
    }

    public EmailOrUsernameAlreadyExistException(final Throwable cause) {
        super(cause);
    }	
}