/**
 * 
 */
package wot.flow.edu.gov.az.persistence.dao;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import wot.flow.edu.gov.az.persistence.entities.aas.PersonEnt;

/**
 * @author Taleh-PC
 * 2017 Nov 4, 2017 2:15:13 PM
 */
public interface PersonRepository extends JpaRepository<PersonEnt, Long> {
   public PersonEnt findByIdAndState (final Long id , final Integer state) ;
   
   @Query("SELECT P FROM PersonEnt P WHERE ((P.fin is not null AND P.fin = :in_fin ) OR (P.idCardsNo is not null AND P .idCardsNo = :in_idCardsNo)) AND state = :in_state")
   public PersonEnt findByFinOrIdCardsNo(final @Param("in_fin") String in_fin, final @Param("in_idCardsNo") String in_idCardsNo, final @Param("in_state") Integer in_state) ;
   
   @Query("SELECT P FROM PersonEnt P WHERE (P.fin is not null AND P.fin = :in_fin ) OR (P.idCardsNo is not null AND P .idCardsNo = :in_idCardsNo) and P.id<> :in_oid")
   public PersonEnt findByFinOrIdCardsNo(final @Param("in_oid") Long in_oid,final @Param("in_fin") String in_fin, final @Param("in_idCardsNo") String in_idCardsNo) ;

/**
 * @param value
 */
  public List<PersonEnt> findAllByState(int value,Pageable pageable);
}
