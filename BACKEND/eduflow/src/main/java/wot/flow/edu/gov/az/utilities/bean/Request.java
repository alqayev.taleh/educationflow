/**
 * 
 */
package wot.flow.edu.gov.az.utilities.bean;

import wot.flow.edu.gov.az.bean.aas.PagingDto;

/**
 * @author Alqayev Taleh
 *
 * 2017 Nov 10, 2017 11:13:41 PM
 */
public class Request {
	private Long authId;

	public Long getAuthId() {
		return authId;
	}

	public void setAuthId(Long authId) {
		this.authId = authId;
	}


}
