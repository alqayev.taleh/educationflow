package wot.flow.edu.gov.az.persistence.dao;

import java.util.Set;

import org.springframework.data.repository.CrudRepository;

import wot.flow.edu.gov.az.persistence.entities.aas.RoleEnt;

/**
 * Created by nydiarra on 06/05/17.
 */
public interface RoleRepository extends CrudRepository<RoleEnt, Long> {
	Set<RoleEnt> findByIdIn(Set<Long> roles);
}
