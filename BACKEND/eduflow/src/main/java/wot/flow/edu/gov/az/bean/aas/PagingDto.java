/**
 * 
 */
package wot.flow.edu.gov.az.bean.aas;

/**
 * @author Alqayev Taleh
 *
 * 2017 Nov 15, 2017 9:16:11 PM
 */
public class PagingDto {
  
	private Integer page;
	
	private Integer size;

	public Integer getPage() {
		return page;
	}

	public void setPage(Integer page) {
		this.page = page;
	}

	public Integer getSize() {
		return size;
	}

	public void setSize(Integer size) {
		this.size = size;
	}
	
	
}
