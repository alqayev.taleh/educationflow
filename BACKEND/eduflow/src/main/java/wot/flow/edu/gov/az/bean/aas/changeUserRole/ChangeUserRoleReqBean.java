/**
 * 
 */
package wot.flow.edu.gov.az.bean.aas.changeUserRole;

import java.util.Set;

import wot.flow.edu.gov.az.bean.aas.RoleDto;
import wot.flow.edu.gov.az.utilities.bean.Request;

/**
 * @author Alqayev Taleh
 *
 * 2017 Nov 16, 2017 11:51:07 PM
 */
public class ChangeUserRoleReqBean extends Request {
    
	private Long id;
	
	private Set<RoleDto> roles;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Set<RoleDto> getRoles() {
		return roles;
	}

	public void setRoles(Set<RoleDto> roles) {
		this.roles = roles;
	}
}
